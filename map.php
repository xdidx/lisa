<html>
	<head>
		<title>Test map</title>
		<style>
		*{margin:0;padding:0;}
		html
		{
			height:100%;
			overflow-x:hidden;			
		}
		#C_map
		{
			position:relative;
			left:400px;
		}
		#D_levelList
		{
			position:fixed;
			top:30px;
			left:0;
			width:400px;
			background-color:#eee;
			overflow-y:auto;
			height:100%;
		}
		#B_save
		{
			position:fixed;
			top:0;
			left:0;
		}
		</style>
		<link rel="stylesheet" media="all" type="text/css" rel="stylesheet" href="includes/main.css" />
	</head>
	<body>
		<input type="button" id="B_save" value="Sauvegarder"/>
		<div id="D_levelList">
		</div>
		<div id="C_map"></div>
		
		<script src="./includes/jquery-2.0.3.min.js"></script>
		<script src="./includes/jquery-ui.min.js"></script>
		<script src="./includes/kinetic-v5.0.1.min.js"></script>
		
		<script>
			
			var mapLevels = Array();
			
			var mapNumber = 0;
			var mapZoom = 50;
			var mapImages = Array();
			
			var mapContainer = new Kinetic.Stage({
				container: 'C_map',
				width: 768,
				height: 1024
			});
			
			$(document).on('change', '.TB_property', function(){
				var levelIndex = $(this).parent().attr('level-index');
				var propertyName = $(this).attr('property');
				console.log(levelIndex, propertyName, mapLevels[levelIndex][propertyName]);
				mapLevels[levelIndex][propertyName] = $(this).val();
			});
			$(document).on('keyup', '.TB_property', function(){
				$(this).change();
			});
			
			$('#B_save')
			.click(function(){
				mapData(true);
			});
			
			function mapData(save)
			{
				var postDatas = { mapLevels : mapLevels };
				if(save)
					postDatas.save = true;
				
				console.log(postDatas);
				
				postDatas = JSON.stringify(postDatas);
				
				$.ajax({
					url: 'webservice/mapDatas.php?dev',
					type: "POST",
					data: { datas : postDatas },
					dataType: "json",
					error : function(what,a,b){
						alert('Probl�me pendant l\'envoi des donn�es');
						console.log(what);
					},
					success : function(datas){
						if(!datas)
						{
							return false;
						}
						else
						{
							$('#D_levelList').html('');
							mapLevels = datas.mapLevels;
							for(var i = 0; i < mapLevels.length; i++)
							{
								var levelOption = document.createElement('option');
								$(levelOption).val(0);
								$(levelOption).html('Niveau');
								var shortcutOption = document.createElement('option');
								$(shortcutOption).val(1);
								$(shortcutOption).html('Raccourcis');
								var deadendOption = document.createElement('option');
								$(deadendOption).val(2);
								$(deadendOption).html('Cul de sac');
								
								var newParentOption = document.createElement('option');
								$(newParentOption).val(0);
								$(newParentOption).html('Aucun');
								var parentsOptions = Array();
								parentsOptions[0] = newParentOption;
								
								for(var j = 0; j < mapLevels.length; j++)
								{
									var newParentOption = document.createElement('option');
									$(newParentOption).val(mapLevels[j].id);
									$(newParentOption).html(mapLevels[j].name);
									parentsOptions[parentsOptions.length] = newParentOption;
								}
							
								var mapLevelDiv = document.createElement('div');
								$(mapLevelDiv).attr('level-index', i);
								
								var mapLevelName = document.createElement('span');
								$(mapLevelName).html(mapLevels[i].name);
								
								if(isNaN(mapLevels[i].x) || mapLevels[i].x == null) mapLevels[i].x = 0;
								var mapLevelX = document.createElement('input');
								$(mapLevelX).attr('property', 'x');
								$(mapLevelX).addClass('TB_property');
								$(mapLevelX).attr('type', 'number');
								$(mapLevelX).val(mapLevels[i].x);
								
								if(isNaN(mapLevels[i].y) || mapLevels[i].y == null) mapLevels[i].y = 0;
								var mapLevelY = document.createElement('input');
								$(mapLevelY).attr('property', 'y');
								$(mapLevelY).addClass('TB_property');
								$(mapLevelY).attr('type', 'number');
								$(mapLevelY).val(mapLevels[i].y);
								
								if(isNaN(mapLevels[i].type) || mapLevels[i].type == null) mapLevels[i].type = 0;
								var mapLevelType = document.createElement('select');
								$(mapLevelType).attr('property', 'type');
								$(mapLevelType).addClass('TB_property');
								$(mapLevelType).append(levelOption);
								$(mapLevelType).append(shortcutOption);
								$(mapLevelType).append(deadendOption);								
								$(mapLevelType).children('option[value='+mapLevels[i].type+']').attr("selected", "selected");
								
								var mapLevelParent = document.createElement('select');
								$(mapLevelParent).attr('property', 'parentId');
								$(mapLevelParent).addClass('TB_property');
								if(isNaN(mapLevels[i].parentId) || mapLevels[i].parentId == null) mapLevels[i].parentId = 0;								
								for(var j = 0; j < parentsOptions.length; j++)
								{
									if($(parentsOptions[j]).val() != mapLevels[i].id)
										$(mapLevelParent).append(parentsOptions[j]);
								}
								
								$(mapLevelDiv).append(mapLevelName);
								$(mapLevelDiv).append(mapLevelType);
								$(mapLevelDiv).append(mapLevelX);
								$(mapLevelDiv).append(mapLevelY);
								$(mapLevelDiv).append(mapLevelParent);
								$('#D_levelList').append(mapLevelDiv);
							}
						}
					}
				});
			}
			
			$(window).load(function(){
				loadImages(drawMap);
				mapData();
			})
			.bind("mousewheel", function(event, delta) {
				if(event.shiftKey)
				{
					if(event.originalEvent.wheelDelta > 0 && mapZoom < 200)
						mapZoom += 5;
					else if(event.originalEvent.wheelDelta <= 0 && mapZoom > 5)
						mapZoom -= 5;
					
					drawMap(false);
				
					return false;
				}
			});
			
			function loadImages(callback)
			{
				var imageNumber = 0;
				var imageLoaded = 0;
				for(var i = 1; i < 2; i++)
				{
					imageNumber++;
					
					var currentImage = new Image();
					mapImages[mapImages.length] = currentImage;
					currentImage.src = './images/maps/map0'+i+'.jpg';
					
					currentImage.onload = function(){
						imageLoaded++;
						if(imageLoaded == imageNumber)
						{
							//callback(true);
						}
					};
				}
			}
			
			function drawMap(scroll)
			{
				mapContainer.removeChildren();

				if(mapImages[mapNumber])
				{
					mapContainer.setWidth(mapImages[mapNumber].width * (mapZoom / 100));
					mapContainer.setHeight(mapImages[mapNumber].height * (mapZoom / 100));
					
					var backgroundLayer = new Kinetic.Layer();
					var backgroundImage = new Kinetic.Image({
						width : mapContainer.width(),
						height : mapContainer.height(),
						image : mapImages[mapNumber]
					});
					
					backgroundLayer.add(backgroundImage);
					
					mapContainer.add(backgroundLayer);
					mapContainer.draw();
					
					if(scroll)
					{
						$('html, body').animate({
							scrollTop: mapContainer.height()
						}, 500);
					}
				}
			}
		</script>
		
	</body>
</html>
