var content = Array();
var saved = true;

var elementHasImage = false;
var anchorWidth = 20;
var showAnchor = true;

var dragging = Array();
dragging['hitboxPoints'] = Array();
dragging['layoutElements'] =  Array();
dragging['layoutGroups'] =  Array();
dragging['groupElements'] =  Array();

var selected = Array();
selected['levels'] = Array();
selected['levelLayouts'] = Array();
selected['layoutElements'] = Array();
selected['elements'] = Array();
selected['elementHitboxes'] = Array();
selected['elementAssets'] = Array();
selected['hitboxes'] = Array();
selected['hitboxPoints'] = Array();
selected['assets'] = Array();
selected['groups'] = Array();
selected['groupElements'] = Array();
selected['layoutGroups'] = Array();
//selected['scripts'] = Array();
//selected['scriptSteps'] = Array();

var dependencies = Array();
dependencies['levelLayouts'] = 'levels';
dependencies['layoutElements'] = 'levelLayouts';
dependencies['layoutGroups'] = 'levelLayouts';
dependencies['hitboxPoints'] = 'hitboxes';
dependencies['elementAssets'] = 'elements';
dependencies['elementHitboxes'] = 'elements';
dependencies['groupElements'] = 'groups';
//dependencies['scriptSteps'] = 'scripts';


var parentField = Array();
parentField['levelLayouts'] = 'levelId';
parentField['layoutElements'] = 'layoutId';
parentField['layoutGroups'] = 'layoutId';
parentField['hitboxPoints'] = 'hitboxId';
parentField['elementAssets'] = 'elementId';
parentField['elementHitboxes'] = 'elementId';
parentField['groupElements'] = 'groupId';

var copiedBeforeSave = Array();
copiedBeforeSave['layoutGroups'] = 0;
copiedBeforeSave['layoutElements'] = 0;

var lastSelectionnedList = '';

var levelZoom = 80;
var hitBoxZoomRatio = 1;
var zooming = false;
var levelScollX = 0;
var levelScollY = 0;
var isLoadingElement = false;

var startPoint = false;
var mouseShift = Array();

var minCategoryWidth = 220;
var minCategoryHeight = 40;

var copied = false;

var rightClicListItemIdx = false;

function getDatas(save)//GET DATAS FROM DB
{	
	var postDatas = Array();
	if(save)
	{
		postDatas = content;
		postDatas['save'] = true;
		postDatas['openCategories'] = getOpenCatgories();
	}
	
	loading(true);

	var ancienSelected = Array();		
	for(var index in selected)
	{
		ancienSelected[index] = selected[index];
	}	

	var postDonnees = new Object();		
	postDonnees['datas'] = JSON.stringify(postDatas);
	
	var url = './webservice/getDatas.php';
	if(location.search == '?dev')
		url += '?dev';
		
	try {
		
		$.ajax({
			url: url,
			type: "POST",
			data: postDonnees,
			dataType: "json",
			error : function(what,a){
				alert('Probl�me pendant l\'envoi des donn�es');
				console.log(what.responseText);
				loading(false);
			},
			success : function(datas){
				if(!datas)
				{
					return false;
				}
				else
				{
					content = datas;
					var openCategories = content.openCategories;
					delete content.openCategories;
					
					loadImages(function(){
					
						/*
						for(var whichList in content)
						{
							refreshList(whichList);
						}
						*/
						
						refreshList('levels');
						refreshList('groups');
						refreshList('elements');
						refreshList('hitboxes');
						refreshList('assets');
							
						saved = true;
						
						//DRAG'N'DROP ELEMENTS => LEVEL
						$('.D_elements')
						.draggable({ revert: true, stop : function(e){
							var mousePosition = getRelativeMousePosition(e, $('#C_levelImage'));
							if(mousePosition.x > 0 && mousePosition.y > 0 && mousePosition.y < $('#C_levelImage').height() && mousePosition.x < $('#C_levelImage').width())
							{
								var defaultDatas = getDefaultDatasToAdd('layoutElements');
								defaultDatas.x = mousePosition.x;
								defaultDatas.y = mousePosition.y;		
								var parentElement = content['elements'][indexOutParent('layoutElements', $(this).index())];
								defaultDatas.elementId = parentElement.id;
								defaultDatas.name = parentElement.name;
								defaultDatas.width = parentElement.width;
								defaultDatas.height = parentElement.height;		
								defaultDatas.anchorX = parentElement.width/2;
								defaultDatas.anchorY = parentElement.height/2;
								
								updateList('layoutElements', 'add', defaultDatas);			
							}
						}});
							
						for(var index in ancienSelected)
						{
							if(copiedBeforeSave[index] && copiedBeforeSave[index].length)
							{
								for(var i = 0; i < copiedBeforeSave[index].length; i++)
								{
									selected[index][i] = copiedBeforeSave[index][i] + copiedBeforeSave[index].length;
								}
							}
							else if(ancienSelected[index].length)
							{
								if(ancienSelected[index].length == 1)
								{
									selectListItem(index, ancienSelected[index][0]);
									
								}else if(ancienSelected[index].length > 1)
								{
									selected[index] = ancienSelected[index];
								}
							}
						}						
						copiedBeforeSave = Array();
						
						//OPEN CATEGORIES
						if(openCategories)
						{
							for(var i = 0; i < openCategories.length; i++)
							{
								var currentCate = $('.categorie').eq(openCategories[i] - 1);
								maximizeCategory(currentCate);
							}
						}
						/*
						setTimeout(function(){
							$(window).resize();
						}, 500);
						*/
					});
					
					
				}
				
				loading(false);
			}
		});
	} catch(err) {
		document.write("<h3>Probl�me avec le PHP. Laisses cette page ouverte et appelles un developpeur !</h3>", err);
	}
}
function getDefaultDatasToAdd(currentList)//RETURN OBJECT WITH DEFAULT DATA TO SEND IN DB
{
	var defaultDatas = new Object();
	defaultDatas.x = 0;
	defaultDatas.y = 0;
	defaultDatas.width = 0;
	defaultDatas.height = 0;
	defaultDatas.rotation = 0;
	defaultDatas.color = 'blue';
	defaultDatas.hitboxId = 0;
	defaultDatas.modeleId = 0;
	defaultDatas.type = 0;
	defaultDatas.loop = 0;
	defaultDatas.inverse = 0;
	defaultDatas.name = 'Sans nom';
	defaultDatas.scriptId  = 0;
	defaultDatas.scaleX = 1;
	defaultDatas.scaleY = 1;
	defaultDatas.anchorX = 0;
	defaultDatas.anchorY = 0;
	defaultDatas.breakable = 0;
	defaultDatas.damage = 0;
	defaultDatas.collectible = 0;
	defaultDatas.sensor = 0;
	defaultDatas.restitution = 0.9;
	defaultDatas.levelsucces = 0;
	defaultDatas['export'] = 0;
	defaultDatas.actionactive = 0;
	defaultDatas.actionpositionx = 0;
	defaultDatas.actionpositiony = 0;
	defaultDatas.actionrotation = 0;
	defaultDatas.actiontime = 1;
	defaultDatas.actionloop = 1;
	defaultDatas.actionreverse = 1;
	defaultDatas.actionbeforeactive = 0;
	defaultDatas.actionbeforereverse = 0;
	defaultDatas.weight = 0;
	defaultDatas.launcher = 0;
	
	if(currentList == 'hitboxPoints')
	{
		defaultDatas.x = $('#C_hitboxImage').width() / (2 * hitBoxZoomRatio);
		defaultDatas.y = $('#C_hitboxImage').height() / (2 * hitBoxZoomRatio);
		defaultDatas.name = 'Point '+content[currentList].length;
		defaultDatas.hitboxId = content['hitboxes'][selected['hitboxes'][0]].id;
		defaultDatas.weight = content['hitboxPoints'][selected['hitboxPoints'][0]].weight*1 + 1;
		
		for(var i = 0; i < content['hitboxPoints'].length; i++)
		{
			if(content['hitboxPoints'][i].hitboxId == defaultDatas.hitboxId && content['hitboxPoints'][i].weight >= defaultDatas.weight)
				content['hitboxPoints'][i].weight++;
		}
	}
	
	if(currentList == 'levelLayouts')
		defaultDatas.levelId = content['levels'][selected['levels'][0]].id;
	
	if(currentList == 'scriptSteps')
		defaultDatas.scriptId = content['scripts'][selected['scripts'][0]].id;
	
	if(currentList == 'layoutElements')
	{
		defaultDatas.layoutId = content['levelLayouts'][selected['levelLayouts'][0]].id;
		defaultDatas.elementId = $('#S_newElementsList').val();
		var parentElement = content['elements'][getIndexById('elements', defaultDatas.elementId)];
		defaultDatas.name = parentElement.name;
	}
	if(currentList == 'layoutGroups')
	{
		defaultDatas.layoutId = content['levelLayouts'][selected['levelLayouts'][0]].id;
		defaultDatas.groupId = $('#S_newGroupsList').val();
		var parentElement = content['groups'][getIndexById('groups', defaultDatas.groupId)];
		defaultDatas.name = parentElement.name;
	}
	
	if(currentList == 'groupElements')
	{
		defaultDatas.groupId = content['groups'][selected['groups'][0]].id;
		defaultDatas.elementId = $('#S_newGroupElementsList').val();
	}
	
	if(currentList == 'elementHitboxes')
	{
		defaultDatas.elementId = content['elements'][selected['elements'][0]].id;
		defaultDatas.hitboxId = $('#S_newElementHitboxesList').val();
		var parentElement = content['hitboxes'][getIndexById('hitboxes', defaultDatas.hitboxId)];
		defaultDatas.width = parentElement.width;
		defaultDatas.height = parentElement.height;
	}
	if(currentList == 'elementAssets')
	{
		defaultDatas.elementId = content['elements'][selected['elements'][0]].id;
		defaultDatas.assetId = $('#S_newElementAssetsList').val();
		var parentElement = content['assets'][getIndexById('assets', defaultDatas.assetId)];
		defaultDatas.width = parentElement.width;
		defaultDatas.height = parentElement.height;
	}
	return defaultDatas;	
}