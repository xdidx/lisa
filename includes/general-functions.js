function getRelativeMousePosition(event, domElement)//GET RELATIVE POSISITION IN [domElement]
{
	var mousePosition = new Object();
	if(domElement.position())
	{
		mousePosition.x = (event.pageX - domElement.position().left);
		mousePosition.y = (event.pageY - domElement.position().top);
	}
	else
	{
		mousePosition.x = event.pageX;
		mousePosition.y = event.pageY;
	}
	return mousePosition;
}
function loading(currentState)//CHANGE LOADING TEXT AND DISABLE PROPERTIES FIELDS
{
	if(currentState == true)
	{
		$('#shadow, #loadingImage').css({'display':'block', 'opacity':0});
		$('#shadow').stop().animate({'opacity' : 0.5}, 500);
		$('#loadingImage').stop().animate({'opacity' : 1}, 500);
		
		$('#D_statusText').html('Chargement...');
		for(var listName in selected)
		{
			$('.needSelected'+capitalizeFirstLetter(listName)).attr('disabled', 'true');
		}
	}
	else
	{	
		$('#shadow, #loadingImage').stop().animate({'opacity' : 0}, 200, function(){
			$('#shadow, #loadingImage').css({'display':'none'});		
		});
		for(var listName in selected)
		{
			if(selected[listName].length == 1)
				$('.needSelected'+capitalizeFirstLetter(listName)).removeAttr('disabled');
			else
				$('.needSelected'+capitalizeFirstLetter(listName)).attr('disabled', 'true');		
		}
		$('#D_statusText').html('Changements effectu�s');
	}
}
function maximizeCategory(category, dontResize, checkIfOpen)//MAXIMIZE CATEGORY
{
	if(!checkIfOpen || (category.height() != minCategoryHeight && category.width() != minCategoryWidth))
	{
		var totalWidth = 0;
		category.children('.fleft, .container')
		.each(function(){
			if($(this).is(':visible'))
				totalWidth += $(this).outerWidth() + 5;		
		});
		var totalHeight = $(window).height() - $('header').outerHeight() - 30;// (30 => boxes padding and margin)
			
		category.stop().animate({'width' : totalWidth, 'height' : totalHeight}, 200, function(){
		
			//SET HEIGHT TO LISTS
			$('.list')
			.each(function(){
				
				var totalModulesHeight = 0;
				var cateHeight = $(this).parent().parent().height() - 60;
				var listHeight = $(this).height();
				
				$(this).parent().children('.module')
				.each(function(){
					totalModulesHeight += $(this).height() + 30;
				});
				
				var newListHeight = (cateHeight - totalModulesHeight) / $(this).parent().children('.list').length;
				
				$(this).stop().animate({'height' : newListHeight}, 200);
				
			});
			
			if(!dontResize)
				$(window).resize();
		});
	}
}
function getOpenCatgories()//GET OPEN CATEGORIES IN ARRAY
{
	var openCategories = Array();
	$('.categorie')
	.each(function(){
		if($(this).height() != minCategoryHeight && $(this).width() != minCategoryWidth)
			openCategories[openCategories.length] = $(this).index();
	});
	return openCategories;
}
function capitalizeFirstLetter(string)//RETURN THE STRING WITH THE FIRST LETTER CAPITALIZED
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
function closeOptionMenu()//CLOSE RIGHT CLIC MENU
{
	$('.optionMenu').css({'display':'none'});
}
function openOptionMenu(list, index, position)//OPEN RIGHT CLIC MENU
{
	$('.optionMenu').css({'display':'none'});
	$('#'+list+'OptionMenu').css({'display':'block', 'top': position.y, 'left':position.x});
}
function setLastSelectedList(currentList)
{
	lastSelectionnedList = currentList;
	$('.list').css('box-shadow', 'none');
	$('#D_'+currentList+'List').css('box-shadow', '0 0 30px white');
}