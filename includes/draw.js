var internColor = 'black';
var externColor = 'white';

//INIT CANVAS
var levelContainer = new Kinetic.Stage({
	container: 'C_levelImage',
	width: 100,
	height: 100
});

var groupContainer = new Kinetic.Stage({
	container: 'C_groupImage',
	width: 200,
	height: 200
});
var groupLayer = new Kinetic.Layer();
groupContainer.add(groupLayer);

var elementContainer = new Kinetic.Stage({
	container: 'C_elementImage',
	width: 200,
	height: 200
});
var elementLayer = new Kinetic.Layer();
elementContainer.add(elementLayer);

var assetContainer = new Kinetic.Stage({
	container: 'C_assetImage',
	width: 200,
	height: 200
});
var assetLayer = new Kinetic.Layer();
assetContainer.add(assetLayer);

var hitboxContainer = new Kinetic.Stage({
	container: 'C_hitboxImage',
	width: 200,
	height: 200
});
var hitboxLayer = new Kinetic.Layer();
hitboxContainer.add(hitboxLayer);

var elementsLayer = new Kinetic.Layer();
var selectionLayer = new Kinetic.Layer();
var selectionRect = new Kinetic.Rect({
	x : 0,
	y : 0,
	width : 0,
	height : 0,
	opacity : 0.3,
	fill : 'blue',
	stroke : internColor
});

//LOAD IMAGES
var images = Array();
var backgrounds = Array();
var loadedImageNumber = 0;
var imageNumber = 0;
var onLayoutElements = false;

function loadImages(callback)
{
	//assets
	for(var assetNum = 0; assetNum < content['assets'].length; assetNum++)
	{
		var currentAsset = content['assets'][assetNum];
		
		if(currentAsset.src.length > 0)
		{			
			imageNumber++;
			
			images[currentAsset.id] = new Image();
			images[currentAsset.id].onload = function(){
				checkEnd();
			}
			images[currentAsset.id].onerror = function(){
				checkEnd();
			}
			images[currentAsset.id].src = 'images/assets/'+currentAsset.src;
			
			function checkEnd(){
				loadedImageNumber++;
				if(loadedImageNumber == imageNumber)
					callback();
			}
		}
	}
	
	/*
	//background
	for(var i = 0; i < 3; i++)
	{
		backgrounds[i] = new Image();
		backgrounds[i].src = 'images/backgrounds/pigeon-background-0'+(i+1)+'.png';
	}
	*/
	
	if(imageNumber == 0)
		callback();
}

//DRAWING FUNCTIONS
function drawGroup(group, layer, position, scale, showSelected)//DRAW ELEMENT ON CANVAS
{
	for(var elementNum = 0; elementNum < content['groupElements'].length; elementNum++)
	{
		if(group.id == content['groupElements'][elementNum].groupId)
		{
			var currentGroupElements = content['groupElements'][elementNum];
			
			var currentElementId = content['groupElements'][elementNum].elementId;
			var currentGroupId = content['groupElements'][elementNum].groupId;
			var currentElement = content['elements'][getIndexById('elements', currentElementId)];
			var currentGroup = content['groups'][getIndexById('groups', currentGroupId)];
			
			var groupSize = getMaxSizeInGroup(currentGroup);
			var elementSize = getMaxSizeInElement(currentElement);
			
			var elementPosition = { x : currentGroupElements.x, y : currentGroupElements.y};
			var elementScale = { x : currentGroupElements.scaleX, y : currentGroupElements.scaleY};
			var anchor = { x : currentGroupElements.anchorX * elementSize.width, y : currentGroupElements.anchorY * elementSize.height};
			
			if(showSelected)
			{
				var newGroup = new Kinetic.Group({
					name : 'visibleGroup',
					groupElementsIndex : elementNum,
					x : elementPosition.x*1 + position.x*1 - groupSize.x*1,
					y : elementPosition.y*1 + position.y*1 - groupSize.y*1,
					groupSize : groupSize,
					scaleX : elementScale.x * scale.x,
					scaleY : elementScale.y * scale.y,
					offsetX: anchor.x*1,
					offsetY: anchor.y*1,
					rotation : currentGroupElements.rotation,
					draggable: true,
					dragBoundFunc: function(pos) {
						if(currentGraphicGroupElementGroup)
						{
							saved = false;
							if($('#CB_grilleGroupe').is(':checked') 
								&& !isNaN($('#TB_largeurGrilleGroupe').val())
								&& !isNaN($('#TB_hauteurGrilleGroupe').val()))
							{
								var gridValueX = $('#TB_largeurGrilleGroupe').val() * currentGraphicGroupElementGroup.scaleX;
								var gridValueY = $('#TB_hauteurGrilleGroupe').val() * currentGraphicGroupElementGroup.scaleY;
								
								pos.x = Math.round(pos.x / gridValueX) * gridValueX - (currentGraphicGroupElementGroup.groupSize.x % gridValueX);
								pos.y = Math.round(pos.y / gridValueY) * gridValueY - (currentGraphicGroupElementGroup.groupSize.y % gridValueY);
							}
						
							if(dragging['groupElements'].length > 0)
							{
								var graphicLayoutElements = groupLayer.children;
								var groupLayoutElementsIndex = currentGraphicGroupElementGroup.groupElementsIndex;
								
								var dif = { x : 0, y : 0 };								
								if(dragging['groupElements'][groupLayoutElementsIndex])
								{
									dif.x = dragging['groupElements'][groupLayoutElementsIndex].x;
									dif.y = dragging['groupElements'][groupLayoutElementsIndex].y;
								}
							
								for(var i = 0; i < graphicLayoutElements.length; i++)
								{
									var currentLayoutElementIndex = graphicLayoutElements[i].attrs.groupElementsIndex;
									if(dragging['groupElements'][currentLayoutElementIndex])
									{
										if(groupLayoutElementsIndex != currentLayoutElementIndex)
										{
											var newPos = Object();
											newPos.x = pos.x*1 - dragging['groupElements'][currentLayoutElementIndex].x*1 + dif.x*1;
											newPos.y = pos.y*1 - dragging['groupElements'][currentLayoutElementIndex].y*1 + dif.y*1;
											
											graphicLayoutElements[i].setX(newPos.x);
											graphicLayoutElements[i].setY(newPos.y);
											
											content['groupElements'][currentLayoutElementIndex].x = newPos.x / currentGraphicGroupElementGroup.scaleX;
											content['groupElements'][currentLayoutElementIndex].y = newPos.y / currentGraphicGroupElementGroup.scaleY;
										}
										else
										{
											content['groupElements'][currentLayoutElementIndex].x = (pos.x + currentGraphicGroupElementGroup.groupSize.x) / currentGraphicGroupElementGroup.scaleX;
											content['groupElements'][currentLayoutElementIndex].y = (pos.y + currentGraphicGroupElementGroup.groupSize.y) / currentGraphicGroupElementGroup.scaleY;
										}
									}
								}
								
								refreshProperties('groupElements');
								
							}
							
						}
						return pos;
					}
				});
				
				//DRAG EVENT
				newGroup.on('mouseover', function(event) {
					onLayoutElements = true;
				});
				newGroup.on('mouseout', function(event) {
					onLayoutElements = false;
				});
				newGroup.on('mousedown', function(event) {
					currentGraphicGroupElementGroup = event.targetNode.parent.attrs;
					draggingGroupElementIndex = currentGraphicGroupElementGroup.groupElementsIndex;
				
				
					var currentIsSeleted = false;
					for(var i = 0; i < selected['groupElements'].length; i++)
					{
						if(selected['groupElements'][i] == draggingGroupElementIndex)
						{
							currentIsSeleted = true;
							break;
						}
					}

					if(!currentIsSeleted)
					{
						selected['groupElements'] = Array();
						
						selectListItem('groupElements', draggingGroupElementIndex, 'dontRefresh');
						setLastSelectedList('groupElements');
						
						groupLayer.find('.visibleGroup')
						.each(function(currentVisibleGroup){
							if(currentVisibleGroup.attrs.groupElementsIndex == draggingGroupElementIndex)
							{
								currentVisibleGroup.find('.strokeSelectionRect').show();
							}
							else
							{
								currentVisibleGroup.find('.strokeSelectionRect').hide();									
							}
						});
					}
					
					if(event.button == 2)
					{
						// openOptionMenu('groupElements', draggingGroupElementIndex, {x : event.pageX, y : event.pageY});
						// currentGraphicGroupElementGroup = false;
					}
					else
					{
						// var currentLayoutElement = content['groupElements'][draggingGroupElementIndex];
						// if(event.altKey)
						// {
							// if(selected['groupElements'].length > 0)
							// {
								// for(var i = 0; i < selected['groupElements'].length; i++)
								// {
									// var curIdx = selected['groupElements'][i];
									// var currentSelectedLayoutElement = content['groupElements'][curIdx];
									
									// var defaultDatas = getDefaultDatasToAdd('groupElements');
									// defaultDatas.elementId = currentSelectedLayoutElement.elementId;
									// defaultDatas.layoutId = currentSelectedLayoutElement.layoutId;
									// defaultDatas.x = currentSelectedLayoutElement.x;
									// defaultDatas.y = currentSelectedLayoutElement.y;
									// defaultDatas.scaleX = currentSelectedLayoutElement.scaleX;
									// defaultDatas.scaleY = currentSelectedLayoutElement.scaleY;
									// defaultDatas.anchorX = currentSelectedLayoutElement.anchorX;
									// defaultDatas.anchorY = currentSelectedLayoutElement.anchorY;
									// defaultDatas.rotation = currentSelectedLayoutElement.rotation;
									
									// updateList('groupElements', 'add', defaultDatas, true);
								// }
							// }
							
							// event.targetNode.parent.fire('mouseup');
							// copiedBeforeSave['groupElements'] = selected['groupElements'];
							// getDatas('save');
						// }
						// else
						// {
						
							if(selected['groupElements'].length > 0)
							{
								dragging['groupElements'] = Array();
								for(var i = 0; i < selected['groupElements'].length; i++)
								{
									var curIdx = selected['groupElements'][i];									
									dragging['groupElements'][curIdx] = new Object();
									dragging['groupElements'][curIdx].x = event.offsetX - (content['groupElements'][curIdx].x * currentGraphicGroupElementGroup.scaleX);
									dragging['groupElements'][curIdx].y = event.offsetY - (content['groupElements'][curIdx].y * currentGraphicGroupElementGroup.scaleY);
								}
							}
							else
							{
								startPoint = { x : event.offsetX, y : event.offsetY };
							}
						// }
					}
				});
				newGroup.on('mouseup', function(event) {
					if(currentGraphicGroupElementGroup)
					{
						currentGraphicGroupElementGroup = false;
						refreshScreen('groupElements');
					}
				});
			}
			else
			{
				var newGroup = new Kinetic.Group({
					x : elementPosition.x*1 + position.x*1 - groupSize.x*1,
					y : elementPosition.y*1 + position.y*1 - groupSize.y*1,
					scaleX : elementScale.x * scale.x,
					scaleY : elementScale.y * scale.y,
					offsetX: anchor.x*1,
					offsetY: anchor.y*1,
					rotation : currentGroupElements.rotation
				});
			}
			
			drawElement(currentElement, newGroup, { x : 0, y : 0 }, { x : 0, y : 0 });
			
			if(showSelected)
			{
				//DRAW ANCHOR POINT
				var anchorCircle = new Kinetic.Circle({
					x: anchor.x,
					y: anchor.y,
					radius: 5,
					fill: internColor
				});
				newGroup.add(anchorCircle);

				if(elementNum ==  selected['groupElements'][0])
				{
					var strokeSelectionRect = new Kinetic.Rect({
						isSelectionRect : true,
						x: newGroup.getX(),
						y: newGroup.getY(),
						width: elementSize.width,
						height: elementSize.height,
						stroke: internColor,
						strokeWidth : 4
					});
					strokeSelectionRect.setX(strokeSelectionRect.attrs.strokeWidth / 2);
					strokeSelectionRect.setY(strokeSelectionRect.attrs.strokeWidth / 2);
					strokeSelectionRect.setWidth(strokeSelectionRect.attrs.width - strokeSelectionRect.attrs.strokeWidth);
					strokeSelectionRect.setHeight(strokeSelectionRect.attrs.height - strokeSelectionRect.attrs.strokeWidth);
					newGroup.add(strokeSelectionRect);
				}
			}
			
			layer.add(newGroup);
		}
	}
}
function drawElement(element, layer, position, scale)//DRAW ELEMENT ON CANVAS
{	
	for(var assetNum = 0; assetNum < content['elementAssets'].length; assetNum++)
	{		
		if(element.id == content['elementAssets'][assetNum].elementId)
		{		
			var currentElementAssets = content['elementAssets'][assetNum];
			var currentAssetId = content['elementAssets'][assetNum].assetId;
			var currentAsset = content['assets'][getIndexById('assets', currentAssetId)];
			var assetPosition = { x : currentElementAssets.x*1 + position.x*1, y : currentElementAssets.y*1 + position.y*1};		
			var assetScale = { x : currentElementAssets.scaleX, y : currentElementAssets.scaleY};
			
			drawAsset(currentAsset, layer, assetPosition, assetScale);
		}
	}
	
	for(var hitboxNum = 0; hitboxNum < content['elementHitboxes'].length; hitboxNum++)
	{
		if(element.id == content['elementHitboxes'][hitboxNum].elementId)
		{
			var currentElementHitboxes = content['elementHitboxes'][hitboxNum];
			var currentHitboxId = content['elementHitboxes'][hitboxNum].hitboxId;
			var currentHitbox = content['hitboxes'][getIndexById('hitboxes', currentHitboxId)];
			var hitboxPosition = { x : currentElementHitboxes.x*1 + position.x*1, y : currentElementHitboxes.y*1 + position.y*1};
			var hitboxScale = { x : currentElementHitboxes.scaleX, y : currentElementHitboxes.scaleY};
			
			drawHitbox(currentHitbox, layer, hitboxPosition, hitboxScale);
		}
	}
}
function drawAsset(asset, layer, position, scale)//DRAW ELEMENT ON CANVAS
{
	if(images[asset.id])
	{
		var K_asset = new Kinetic.Image({
			x: position.x,
			y: position.y,
			image: images[asset.id],
			width: asset.width,
			height: asset.height
		});
		layer.add(K_asset);
	}
}
function drawHitbox(hitbox, layer, position, scale)//DRAW HITBOX ON CANVAS
{
	var fillColor = 'blue';
	var strokeColor = internColor;
	var alpha = 0.3;
		
	if(hitbox.type == 0)//RECTANGLE
	{	
		var rect = new Kinetic.Rect({
			x: hitbox.x*1 + position.x*1,
			y: hitbox.y*1 + position.y*1,
			width: hitbox.width,
			height: hitbox.height,
			opacity : alpha,
			fill : fillColor,
			stroke: strokeColor
		});

		layer.add(rect);
	}
	else if(hitbox.type == 1)//ROND
	{
		var circle = new Kinetic.Circle({
			x: hitbox.x*1 + position.x*1,
			y: hitbox.y*1 + position.y*1,
			radius: hitbox.width/2,
			opacity : alpha,
			fill : fillColor,
			stroke: strokeColor
		});
		
		layer.add(circle);		
	}
	else if(hitbox.type == 2)//POLYGONE
	{
		//DRAW LINES
		var currentHitboxPoints = getCurrentHitboxPoints(hitbox, position);
		
		var poly = new Kinetic.Line({
			points: currentHitboxPoints,
			opacity : alpha,
			fill: fillColor,
			stroke: strokeColor,
			closed: true
		});
		
		layer.add(poly);
	}
}

var selectionStartPoint = false;
var currentGraphicGroupElementGroup;
var currentGraphicElementGroup;
var currentGraphicGroupGroup;
var draggingLayoutElementIndex;
var draggingLayoutGroupIndex;

//DRAW SELECTED ITEMS
function drawSelectedLevel()//DRAW LEVEL
{
	levelContainer.setWidth(768 * (levelZoom/100));
	levelContainer.setHeight(1024 * (levelZoom/100));
	levelContainer.removeChildren();
	
	//BACKGROUND RECT
	var backgroundLayer = new Kinetic.Layer();
	var backgroundRect = new Kinetic.Rect({
		width : levelContainer.attrs.width,
		height : levelContainer.attrs.height,
		fill : externColor
	});
	backgroundLayer.add(backgroundRect);
	levelContainer.add(backgroundLayer);
	
	if(selected['levels'].length)
	{
		/*
		//draw background
		var backgroundNumber = Math.floor((content['levels'].length - selected['levels'][0] - 1) / 5);
		if(backgrounds[backgroundNumber])
		{		
			var backgroundImage = new Kinetic.Image({
				width : levelContainer.attrs.width,
				height : levelContainer.attrs.height,
				image : backgrounds[backgroundNumber]
			});
			backgroundLayer.add(backgroundImage);
		}
		*/
		
		var levelScale = { x : levelZoom / 100, y : levelZoom / 100 };
		 
		//DRAW GRID
		if($('#CB_grille').is(':checked') 
			&& !isNaN($('#TB_largeurGrille').val())
			&& !isNaN($('#TB_hauteurGrille').val()))
		{
			var gridLayer = new Kinetic.Layer();
			
			for(var i = 0; i < levelContainer.attrs.width; i += $('#TB_largeurGrille').val() * levelScale.x)
			{
				var currentLine = new Kinetic.Line({
					points: [i, 0, i, levelContainer.attrs.height],
					stroke: internColor
				});
				
				gridLayer.add(currentLine);
			
			}
			for(var i = 0; i < levelContainer.attrs.height; i += $('#TB_hauteurGrille').val() * levelScale.y)
			{
				var currentLine = new Kinetic.Line({
					points: [0, i, levelContainer.attrs.width, i],
					stroke: internColor
				});
				
				gridLayer.add(currentLine);
			}
			
			levelContainer.add(gridLayer);
		}
		
		//DRAW ELEMENTS		
		var currentLevel = content['levels'][selected['levels'][0]];
		for(var layerNum = content['levelLayouts'].length-1; layerNum >= 0; layerNum--)
		{
			var isVisible = $('.D_levelLayouts input').eq(indexInParent('levelLayouts', layerNum)).is(':checked');
			
			if(currentLevel.id == content['levelLayouts'][layerNum].levelId && isVisible)
			{
				var currentGraphicLayoutGroup = new Kinetic.Group();
			
				//FOREACH LAYOUT'S ELEMENTS
				for(var layoutElementNum = 0; layoutElementNum < content['layoutElements'].length; layoutElementNum++)
				{
					if(content['levelLayouts'][layerNum].id == content['layoutElements'][layoutElementNum].layoutId)
					{
						var currentLayoutElement = content['layoutElements'][layoutElementNum];
						var currentElementId = content['layoutElements'][layoutElementNum].elementId;
						var currentElement = content['elements'][getIndexById('elements', currentElementId)];
						
						var elementSize = getMaxSizeInElement(currentElement);
						
						var position = { x : currentLayoutElement.x , y : currentLayoutElement.y};	
						var scale = { x : currentLayoutElement.scaleX * levelScale.x, y : currentLayoutElement.scaleY * levelScale.y};		
						var anchor = { x : currentLayoutElement.anchorX * elementSize.width, y : currentLayoutElement.anchorY * elementSize.height};
						//var anchor = { x : currentLayoutElement.anchorX, y : currentLayoutElement.anchorY};
						
						var group = new Kinetic.Group({
							name : 'visibleGroup',
							layoutElementsIndex : layoutElementNum,
							x : position.x * scale.x,
							y : position.y * scale.y,
							offset: anchor,
							rotation : currentLayoutElement.rotation,
							scale : scale,
							draggable: true,
							dragBoundFunc: function(pos) {
								if(currentGraphicElementGroup)
								{
									saved = false;
									if($('#CB_grille').is(':checked') 
										&& !isNaN($('#TB_largeurGrille').val())
										&& !isNaN($('#TB_hauteurGrille').val()))
									{
										var gridValueX = $('#TB_largeurGrille').val() * currentGraphicElementGroup.scaleX;
										var gridValueY = $('#TB_hauteurGrille').val() * currentGraphicElementGroup.scaleY;
									
										pos.x = Math.round(pos.x / gridValueX) * gridValueX;
										pos.y = Math.round(pos.y / gridValueY) * gridValueY;
									}
								
									if(dragging['layoutElements'].length > 0)
									{
										for(var j = 0; j < elementsLayer.children.length; j++)
										{
											var graphicLayoutElements = elementsLayer.children[j].children;										
											var groupLayoutElementsIndex = currentGraphicElementGroup.layoutElementsIndex;
											
											var dif = { x : 0, y : 0 };
											if(dragging['layoutElements'][groupLayoutElementsIndex])
											{
												dif.x = dragging['layoutElements'][groupLayoutElementsIndex].x;
												dif.y = dragging['layoutElements'][groupLayoutElementsIndex].y;
											}
										
											for(var i = 0; i < graphicLayoutElements.length; i++)
											{
												var currentLayoutElementIndex = graphicLayoutElements[i].attrs.layoutElementsIndex;
												if(dragging['layoutElements'][currentLayoutElementIndex])
												{
													if(currentGraphicElementGroup.layoutElementsIndex != currentLayoutElementIndex)
													{
														var newPos = Object();
														newPos.x = pos.x*1 - dragging['layoutElements'][currentLayoutElementIndex].x*1 + dif.x*1;
														newPos.y = pos.y*1 - dragging['layoutElements'][currentLayoutElementIndex].y*1 + dif.y*1;
														
														graphicLayoutElements[i].setX(newPos.x);
														graphicLayoutElements[i].setY(newPos.y);
														
														content['layoutElements'][currentLayoutElementIndex].x = newPos.x / currentGraphicElementGroup.scaleX;
														content['layoutElements'][currentLayoutElementIndex].y = newPos.y / currentGraphicElementGroup.scaleY;
													}
													else
													{
														content['layoutElements'][currentLayoutElementIndex].x = pos.x / currentGraphicElementGroup.scaleX;
														content['layoutElements'][currentLayoutElementIndex].y = pos.y / currentGraphicElementGroup.scaleY;
													}
												}
											}
										
										}
										
										refreshProperties('layoutElements');
										
									}
									
								}
								return pos;
							}
						});
						
						//DRAG EVENT
						group.on('mouseover', function(event) {
							var test = event.targetNode.parent.attrs;
							onLayoutElements = true;
						});
						group.on('mouseout', function(event) {
							onLayoutElements = false;
						});
						group.on('mousedown', function(event) {
							currentGraphicElementGroup = event.targetNode.parent.attrs;
							draggingLayoutElementIndex = currentGraphicElementGroup.layoutElementsIndex;
						
						
							var currentIsSeleted = false;
							for(var i = 0; i < selected['layoutElements'].length; i++)
							{
								if(selected['layoutElements'][i] == draggingLayoutElementIndex)
								{
									currentIsSeleted = true;
									break;
								}
							}

							if(!currentIsSeleted)
							{
								selected['layoutElements'] = Array();
								
								selectListItem('layoutElements', draggingLayoutElementIndex, 'dontRefresh');
								setLastSelectedList('layoutElements');
								
								elementsLayer.find('.visibleGroup')
								.each(function(currentVisibleGroup){
									if(currentVisibleGroup.attrs.layoutElementsIndex == draggingLayoutElementIndex)
									{
										currentVisibleGroup.find('.strokeSelectionRect').show();
									}
									else
									{
										currentVisibleGroup.find('.strokeSelectionRect').hide();									
									}
								});
							}
							
							if(event.button == 2)
							{
								openOptionMenu('layoutElements', draggingLayoutElementIndex, {x : event.pageX, y : event.pageY});
								currentGraphicElementGroup = false;
							}
							else
							{
								var currentLayoutElement = content['layoutElements'][draggingLayoutElementIndex];
								if(event.altKey)
								{
									if(selected['layoutElements'].length > 0)
									{
										for(var i = 0; i < selected['layoutElements'].length; i++)
										{
											var curIdx = selected['layoutElements'][i];
											var currentSelectedLayoutElement = content['layoutElements'][curIdx];
											
											var defaultDatas = getDefaultDatasToAdd('layoutElements');
											defaultDatas.elementId = currentSelectedLayoutElement.elementId;
											defaultDatas.layoutId = currentSelectedLayoutElement.layoutId;
											defaultDatas.x = currentSelectedLayoutElement.x;
											defaultDatas.y = currentSelectedLayoutElement.y;
											defaultDatas.scaleX = currentSelectedLayoutElement.scaleX;
											defaultDatas.scaleY = currentSelectedLayoutElement.scaleY;
											defaultDatas.anchorX = currentSelectedLayoutElement.anchorX;
											defaultDatas.anchorY = currentSelectedLayoutElement.anchorY;
											defaultDatas.rotation = currentSelectedLayoutElement.rotation;
											
											updateList('layoutElements', 'add', defaultDatas, true);
										}
									}
									
									event.targetNode.parent.fire('mouseup');
									copiedBeforeSave['layoutElements'] = selected['layoutElements'];
									getDatas('save');
								}
								else
								{
								
									if(selected['layoutElements'].length > 0)
									{
										dragging['layoutElements'] = Array();
										for(var i = 0; i < selected['layoutElements'].length; i++)
										{
											var curIdx = selected['layoutElements'][i];									
											dragging['layoutElements'][curIdx] = new Object();
											dragging['layoutElements'][curIdx].x = event.offsetX - (content['layoutElements'][curIdx].x * currentGraphicElementGroup.scaleX);
											dragging['layoutElements'][curIdx].y = event.offsetY - (content['layoutElements'][curIdx].y * currentGraphicElementGroup.scaleY);
										}
									}
									else
									{
										startPoint = { x : event.offsetX, y : event.offsetY };
									}
								}
							}
						});
						group.on('mouseup', function(event) {
							if(currentGraphicElementGroup)
							{
								currentGraphicElementGroup = false;
								refreshScreen('layoutElements');
							}
						});
						
						//DRAW ELEMENT
						drawElement(currentElement, group, { x : 0, y : 0 }, { x : 1, y : 1 });
						
						if(showAnchor)
						{
							//DRAW ANCHOR POINT
							var anchorCircle = new Kinetic.Circle({
								x: anchor.x,
								y: anchor.y,
								radius: 5,
								fill: internColor
							});
							group.add(anchorCircle);
						}
						
						//DRAW SELECTION RECTANGLE						
						var elementSize = getMaxSizeInElement(currentElement);
						var strokeSelectionRect = new Kinetic.Rect({
							name : 'strokeSelectionRect',
							isSelectionRect : true,
							x: elementSize.x,
							y: elementSize.y,
							width: elementSize.width,
							height: elementSize.height,
							stroke: internColor,
							strokeWidth : 4
						});
						
						strokeSelectionRect.setX(strokeSelectionRect.attrs.strokeWidth / 2);
						strokeSelectionRect.setY(strokeSelectionRect.attrs.strokeWidth / 2);
						strokeSelectionRect.setWidth(strokeSelectionRect.attrs.width - strokeSelectionRect.attrs.strokeWidth);
						strokeSelectionRect.setHeight(strokeSelectionRect.attrs.height - strokeSelectionRect.attrs.strokeWidth);
						
						group.add(strokeSelectionRect);
						strokeSelectionRect.hide();
						
						for(var i = 0; i < selected['layoutElements'].length; i++)
							if(selected['layoutElements'][i] == layoutElementNum)
								strokeSelectionRect.show();
						
						var rotatedPoint = { x : currentLayoutElement.actionpositionx*1, y : currentLayoutElement.actionpositiony*1 };
						rotatedPoint = getRotatedPoint(-currentLayoutElement.rotation, rotatedPoint);
						
						//DRAW ANIMATION LINE
						var animationLine = new Kinetic.Line({
							name : 'animationLine',
							points : [anchor.x, anchor.y , anchor.x*1 + rotatedPoint.x, anchor.y*1 + rotatedPoint.y],
							stroke: 'blue',
							strokeWidth : 4
						});
						var animationPoint = new Kinetic.Circle({
							name : 'animationPoint',
							fill : 'blue',
							x : anchor.x*1 + rotatedPoint.x,
							y : anchor.y*1 + rotatedPoint.y,
							radius : 6,
							draggable : true,
							dragBoundFunc: function(pos) {
								if(currentGraphicElementGroup)
								{
									var layoutElementsIndex = currentGraphicElementGroup.layoutElementsIndex;
									content['layoutElements'][layoutElementsIndex].actionpositionx = (pos.x / currentGraphicElementGroup.scaleX)- content['layoutElements'][layoutElementsIndex].x;
									content['layoutElements'][layoutElementsIndex].actionpositiony = (pos.y / currentGraphicElementGroup.scaleY) - content['layoutElements'][layoutElementsIndex].y;
									refreshProperties('layoutElements');
								}
								return pos;
							}
						});						
						animationPoint.on('mouseover', function(event) {
							onLayoutElements = true;
						});
						animationPoint.on('mousedown', function(event) {
							currentGraphicElementGroup = event.targetNode.parent.attrs;
							if(isNaN(currentGraphicElementGroup.layoutElementsIndex))
								currentGraphicElementGroup = event.targetNode.parent.parent.attrs;
						});
						animationPoint.on('mouseup', function(event) {
							currentGraphicElementGroup = false;
							refreshScreen('levels');
						});
						animationPoint.on('mouseout', function(event) {
							onLayoutElements = false;
						});
						
						if(currentLayoutElement.actionactive != 1)
						{
							animationPoint.hide();
							animationLine.hide();
						}
						
						group.add(animationLine);
						group.add(animationPoint);
						
						currentGraphicLayoutGroup.add(group);
						
					}
				}
				
				//FOREACH LAYOUT'S GROUPS
				for(var layoutGroupNum = 0; layoutGroupNum < content['layoutGroups'].length; layoutGroupNum++)
				{
					if(content['levelLayouts'][layerNum].id == content['layoutGroups'][layoutGroupNum].layoutId)
					{						
						var currentLayoutGroup = content['layoutGroups'][layoutGroupNum];
						var currentGroupId = content['layoutGroups'][layoutGroupNum].groupId;
						var currentGroup = content['groups'][getIndexById('groups', currentGroupId)];
						
						var groupSize = getMaxSizeInGroup(currentGroup);
						var position = { x : currentLayoutGroup.x , y : currentLayoutGroup.y};	
						var scale = { x : currentLayoutGroup.scaleX * levelScale.x, y : currentLayoutGroup.scaleY * levelScale.y};		
						var anchor = { x : currentLayoutGroup.anchorX * groupSize.width, y : currentLayoutGroup.anchorY * groupSize.height};
						//var anchor = { x : currentLayoutGroup.anchorX, y : currentLayoutGroup.anchorY};
						
						var group = new Kinetic.Group({
							name : 'visibleGroup',
							layoutGroupsIndex : layoutGroupNum,
							x : position.x * scale.x,
							y : position.y * scale.y,
							offset: anchor,
							rotation : currentLayoutGroup.rotation,
							scale : scale,
							draggable: true,
							dragBoundFunc: function(pos) {
								if(currentGraphicGroupGroup)
								{
									saved = false;
									if($('#CB_grille').is(':checked') 
										&& !isNaN($('#TB_largeurGrille').val())
										&& !isNaN($('#TB_hauteurGrille').val()))
									{
										var gridValueX = $('#TB_largeurGrille').val() * currentGraphicGroupGroup.scaleX;
										var gridValueY = $('#TB_hauteurGrille').val() * currentGraphicGroupGroup.scaleY;
									
										pos.x = Math.round(pos.x / gridValueX) * gridValueX;
										pos.y = Math.round(pos.y / gridValueY) * gridValueY;
									}
								
									if(dragging['layoutGroups'].length > 0)
									{
										for(var j = 0; j < elementsLayer.children.length; j++)
										{
											var graphicLayoutElements = elementsLayer.children[j].children;
											var groupLayoutElementsIndex = currentGraphicGroupGroup.layoutGroupsIndex;
											
											var dif = { x : 0, y : 0 };
											if(dragging['layoutGroups'][groupLayoutElementsIndex])
											{
												dif.x = dragging['layoutGroups'][groupLayoutElementsIndex].x;
												dif.y = dragging['layoutGroups'][groupLayoutElementsIndex].y;
											}
										
											for(var i = 0; i < graphicLayoutElements.length; i++)
											{
												var currentLayoutElementIndex = graphicLayoutElements[i].attrs.layoutGroupsIndex;
												if(dragging['layoutGroups'][currentLayoutElementIndex])
												{
													if(currentGraphicGroupGroup.layoutGroupsIndex != currentLayoutElementIndex)
													{
														var newPos = Object();
														newPos.x = pos.x*1 - dragging['layoutGroups'][currentLayoutElementIndex].x*1 + dif.x*1;
														newPos.y = pos.y*1 - dragging['layoutGroups'][currentLayoutElementIndex].y*1 + dif.y*1;
														
														graphicLayoutElements[i].setX(newPos.x);
														graphicLayoutElements[i].setY(newPos.y);
														
														content['layoutGroups'][currentLayoutElementIndex].x = newPos.x / currentGraphicGroupGroup.scaleX;
														content['layoutGroups'][currentLayoutElementIndex].y = newPos.y / currentGraphicGroupGroup.scaleY;
													}
													else
													{
														content['layoutGroups'][currentLayoutElementIndex].x = pos.x / currentGraphicGroupGroup.scaleX;
														content['layoutGroups'][currentLayoutElementIndex].y = pos.y / currentGraphicGroupGroup.scaleY;
													}
												}
											}
										}
										
										refreshProperties('layoutGroups');
										
									}
									
								}
								return pos;
							}
						});
						
						//DRAG EVENT
						group.on('mouseover', function(event) {
							var test = event.targetNode.parent.attrs;
							onLayoutElements = true;
						});
						group.on('mouseout', function(event) {
							onLayoutElements = false;
						});
						group.on('mousedown', function(event) {
						
							currentGraphicGroupGroup = event.targetNode.parent.attrs;
							if(isNaN(currentGraphicGroupGroup.layoutGroupsIndex))
								currentGraphicGroupGroup = event.targetNode.parent.parent.attrs;
							
							draggingLayoutGroupIndex = currentGraphicGroupGroup.layoutGroupsIndex;
						
							var currentIsSeleted = false;
							for(var i = 0; i < selected['layoutGroups'].length; i++)
							{
								if(selected['layoutGroups'][i] == draggingLayoutGroupIndex)
								{
									currentIsSeleted = true;
									break;
								}
							}

							if(!currentIsSeleted)
							{
								selected['layoutGroups'] = Array();
								
								selectListItem('layoutGroups', draggingLayoutGroupIndex, 'dontRefresh');
								setLastSelectedList('layoutGroups');
								
								elementsLayer.find('.visibleGroup')
								.each(function(currentVisibleGroup){
									if(currentVisibleGroup.attrs.layoutGroupsIndex == draggingLayoutGroupIndex)
									{
										currentVisibleGroup.find('.strokeSelectionRect').show();
									}
									else
									{
										currentVisibleGroup.find('.strokeSelectionRect').hide();									
									}
								});
							}
							
							if(event.button == 2)
							{
								openOptionMenu('layoutGroups', draggingLayoutGroupIndex, {x : event.pageX, y : event.pageY});
								currentGraphicGroupGroup = false;
							}
							else
							{
								var currentLayoutGroup = content['layoutGroups'][draggingLayoutGroupIndex];
								if(event.altKey)
								{
									if(selected['layoutGroups'].length > 0)
									{
										for(var i = 0; i < selected['layoutGroups'].length; i++)
										{
											var curIdx = selected['layoutGroups'][i];
											var currentSelectedLayoutGroup = content['layoutGroups'][curIdx];
											
											var defaultDatas = getDefaultDatasToAdd('layoutGroups');
											defaultDatas.groupId = currentSelectedLayoutGroup.groupId;
											defaultDatas.layoutId = currentSelectedLayoutGroup.layoutId;
											defaultDatas.x = currentSelectedLayoutGroup.x;
											defaultDatas.y = currentSelectedLayoutGroup.y;
											defaultDatas.scaleX = currentSelectedLayoutGroup.scaleX;
											defaultDatas.scaleY = currentSelectedLayoutGroup.scaleY;
											defaultDatas.anchorX = currentSelectedLayoutGroup.anchorX;
											defaultDatas.anchorY = currentSelectedLayoutGroup.anchorY;
											defaultDatas.rotation = currentSelectedLayoutGroup.rotation;
											
											updateList('layoutGroups', 'add', defaultDatas, true);
										}
									}
									
									copiedBeforeSave['layoutGroups'] = selected['layoutGroups'];
									event.targetNode.parent.fire('mouseup');
									getDatas('save');
								}
								else
								{								
									if(selected['layoutGroups'].length > 0)
									{
										dragging['layoutGroups'] = Array();
										for(var i = 0; i < selected['layoutGroups'].length; i++)
										{
											var curIdx = selected['layoutGroups'][i];									
											dragging['layoutGroups'][curIdx] = new Object();
											dragging['layoutGroups'][curIdx].x = event.offsetX - (content['layoutGroups'][curIdx].x * currentGraphicGroupGroup.scaleX);
											dragging['layoutGroups'][curIdx].y = event.offsetY - (content['layoutGroups'][curIdx].y * currentGraphicGroupGroup.scaleY);
										}
									}
									else
									{
										startPoint = { x : event.offsetX, y : event.offsetY };
									}
								}
							}
						});
						group.on('mouseup', function(event) {
							if(currentGraphicGroupGroup)
							{
								currentGraphicGroupGroup = false;
								refreshScreen('levels');
							}
						});
						
						//DRAW GROUP
						drawGroup(currentGroup, group, { x : 0, y : 0 }, { x : 1, y : 1 });
						
						if(showAnchor)
						{
							//DRAW ANCHOR POINT
							var anchorCircle = new Kinetic.Circle({
								x: anchor.x,
								y: anchor.y,
								radius: 5,
								fill: internColor
							});
							group.add(anchorCircle);
						}
						
						//DRAW SELECTION RECTANGLE
						var strokeSelectionRect = new Kinetic.Rect({
							name : 'strokeSelectionRect',
							isSelectionRect : true,
							width: groupSize.width,
							height: groupSize.height,
							stroke: internColor,
							strokeWidth : 7
						});
						
						strokeSelectionRect.setX(strokeSelectionRect.x() + (strokeSelectionRect.attrs.strokeWidth / 2));
						strokeSelectionRect.setY(strokeSelectionRect.y() + (strokeSelectionRect.attrs.strokeWidth / 2));
						strokeSelectionRect.setWidth(strokeSelectionRect.width() - strokeSelectionRect.attrs.strokeWidth);
						strokeSelectionRect.setHeight(strokeSelectionRect.height() - strokeSelectionRect.attrs.strokeWidth);
						
						group.add(strokeSelectionRect);
						strokeSelectionRect.hide();
						
						for(var i = 0; i < selected['layoutGroups'].length; i++)
							if(selected['layoutGroups'][i] == layoutGroupNum)
								strokeSelectionRect.show();
						
						var rotatedPoint = { x : currentLayoutGroup.actionpositionx*1, y : currentLayoutGroup.actionpositiony*1 };
						rotatedPoint = getRotatedPoint(-currentLayoutGroup.rotation, rotatedPoint);
						
						//DRAW ANIMATION LINE
						var animationLine = new Kinetic.Line({
							name : 'animationLine',
							points : [anchor.x, anchor.y , anchor.x*1 + rotatedPoint.x, anchor.y*1 + rotatedPoint.y],
							stroke: 'blue',
							strokeWidth : 4
						});
						var animationPoint = new Kinetic.Circle({
							name : 'animationPoint',
							fill : 'blue',
							x : anchor.x*1 + rotatedPoint.x,
							y : anchor.y*1 + rotatedPoint.y,
							radius : 6,
							draggable : true,
							dragBoundFunc: function(pos) {
								if(currentGraphicGroupGroup)
								{
									var layoutGroupsIndex = currentGraphicGroupGroup.layoutGroupsIndex;
									content['layoutGroups'][layoutGroupsIndex].actionpositionx = (pos.x / currentGraphicGroupGroup.scaleX)- content['layoutGroups'][layoutGroupsIndex].x;
									content['layoutGroups'][layoutGroupsIndex].actionpositiony = (pos.y / currentGraphicGroupGroup.scaleY) - content['layoutGroups'][layoutGroupsIndex].y;
									refreshProperties('layoutGroups');
								}
								return pos;
							}
						});						
						animationPoint.on('mouseover', function(event) {
							onLayoutElements = true;
						});
						animationPoint.on('mousedown', function(event) {
							currentGraphicGroupGroup = event.targetNode.parent.attrs;
							if(isNaN(currentGraphicGroupGroup.layoutGroupsIndex))
								currentGraphicGroupGroup = event.targetNode.parent.parent.attrs;
						});
						animationPoint.on('mouseup', function(event) {
							currentGraphicGroupGroup = false;
							refreshScreen('levels');
						});
						animationPoint.on('mouseout', function(event) {
							onLayoutElements = false;
						});
						
						if(currentLayoutGroup.actionactive != 1)
						{
							animationPoint.hide();
							animationLine.hide();
						}
						
						group.add(animationLine);
						group.add(animationPoint);
						
						currentGraphicLayoutGroup.add(group);
						
					}					
				}
				
				elementsLayer.add(currentGraphicLayoutGroup);
			}
		}
		
		levelContainer.add(elementsLayer);
	}

	//SELECTION RECT
	selectionLayer.add(selectionRect);
	levelContainer.add(selectionLayer);
	
	//DRAW LEVEL
	levelContainer.draw();
}
function drawSelectedElement()//DRAW SELECTED ELEMENT IN ELEMENT CANVAS
{	
	elementLayer.removeChildren();
	
	var selectedElement = content['elements'][selected['elements'][0]];
	
	if(selectedElement)
	{
		var position = { x : 0, y : 0};		
		var scale = { x : 1, y : 1};
		
		var maxSize = getMaxSizeInElement(selectedElement);
		elementContainer.setWidth(maxSize.width);
		elementContainer.setHeight(maxSize.height);
		maximizeCategory($('#D_elementCreator'), false, 'onlyIfIsOpen');
		drawElement(selectedElement, elementLayer, position, scale);
		elementContainer.draw();
	}
}
function drawSelectedGroup()//DRAW SELECTED GROUP IN GROUP CANVAS
{
	groupLayer.removeChildren();
	
	var selectedGroup = content['groups'][selected['groups'][0]];
	
	if(selectedGroup)
	{
		var position = { x : 0, y : 0};		
		var scale = { x : 1, y : 1};
		
		var maxSize = getMaxSizeInGroup(selectedGroup);
		groupContainer.setWidth(maxSize.width);
		groupContainer.setHeight(maxSize.height);
		maximizeCategory($('#D_groupCreator'), false, 'onlyIfIsOpen');
		
		drawGroup(selectedGroup, groupLayer, position, scale, 'showSelected');
		
		if($('#CB_grilleGroupe').is(':checked') 
			&& !isNaN($('#TB_largeurGrilleGroupe').val())
			&& !isNaN($('#TB_hauteurGrilleGroupe').val()))
		{
			var gridLayer = new Kinetic.Group();
			
			for(var i = maxSize.x; i < maxSize.width; i += $('#TB_largeurGrilleGroupe').val()*1)
			{
				var currentLine = new Kinetic.Line({
					points: [i, 0, i, maxSize.height],
					stroke: internColor
				});
				
				gridLayer.add(currentLine);
			}
			
			for(var i = maxSize.y; i < maxSize.height; i += $('#TB_hauteurGrilleGroupe').val()*1)
			{
				var currentLine = new Kinetic.Line({
					points: [0, i, maxSize.width, i],
					stroke: internColor
				});
				gridLayer.add(currentLine);
			}
			
			groupLayer.add(gridLayer);
		}
		
		groupContainer.draw();
		
	}
}
function drawSelectedAsset()//DRAW SELECTED ASSET IN ASSET CANVAS
{
	assetLayer.removeChildren();
	
	var selectedAsset = content['assets'][selected['assets'][0]];
	if(selectedAsset)
	{
		var position = { x : 0, y : 0};
		var scale = { x : 1, y : 1};
		
		assetContainer.setWidth(selectedAsset.width);
		assetContainer.setHeight(selectedAsset.height);		
		maximizeCategory($('#D_assetCreator'), false, 'onlyIfIsOpen');
		
		drawAsset(selectedAsset, assetLayer, position, scale);
		assetContainer.draw();
	}
}
var actualPoint = false;
function drawSelectedHitbox()//DRAW SELECTED HITBOX IN HITBOX CANVAS
{
	hitboxLayer.removeChildren();
	
	var selectedHitbox = content['hitboxes'][selected['hitboxes'][0]];
	if(selectedHitbox)
	{
		var hitboxMargin = 20;
		var position = { x : hitboxMargin, y : hitboxMargin};
		var scale = { x : 1, y : 1};
		
		if(selectedHitbox.modeleId > 0)
		{
			var modele = content['assets'][getIndexById('assets', selectedHitbox.modeleId)];
			if(modele && modele.src.length > 0)
			{			
				$('#hitboxModelWidth').html(modele.width);
				$('#hitboxModelHeight').html(modele.height);
			
				hitboxContainer.setWidth(images[selectedHitbox.modeleId].width*1 + hitboxMargin*2);
				hitboxContainer.setHeight(images[selectedHitbox.modeleId].height*1 + hitboxMargin*2);
				drawAsset(modele, hitboxLayer, position, scale);
			}
			else
			{
				hitboxContainer.setWidth(200);
				hitboxContainer.setHeight(200);
			}
		}
		else
		{
			hitboxContainer.setWidth(200);
			hitboxContainer.setHeight(200);		
		}
		maximizeCategory($('#D_hitboxCreator'), false, 'onlyIfIsOpen');
		
		drawHitbox(selectedHitbox, hitboxLayer, position, scale);
		
	}
	
	//DRAW ANCHOR POINTS
	var currentHitboxPoints = getCurrentHitboxPoints(selectedHitbox, {x : 0, y : 0});
	for(var i=0; i < currentHitboxPoints.length; i+=2)
	{
		var hitboxPointCircle = new Kinetic.Circle({
			name: 'hitboxPointCircle',
			index : i,
			x: currentHitboxPoints[i]*1 + hitboxMargin,
			y: currentHitboxPoints[i+1]*1 + hitboxMargin,
			radius : 7,
			fill : internColor,
			stroke: internColor,
			draggable : true,
			dragBoundFunc: function(pos) {
				if(actualPoint)
				{
					saved = false;
					var currentPointIndex = actualPoint.index / 2;
						
					var rects = hitboxLayer.find('Rect');
					var circles = hitboxLayer.find('Circle');
					var polygons = hitboxLayer.find('Line');
					if(rects.length)
					{//RECTANGLE						
						if(currentPointIndex == 0)
						{
							var difX = rects[0].attrs.x - pos.x;
							var difY = rects[0].attrs.y - pos.y;
							
							rects[0].setX(pos.x);
							rects[0].setY(pos.y);
							rects[0].setWidth(rects[0].attrs.width*1 + difX*1);
							rects[0].setHeight(rects[0].attrs.height*1 + difY*1);
						}
						else if(currentPointIndex == 1)
						{
							var difY = rects[0].attrs.y - pos.y;
							
							rects[0].setY(pos.y);
							rects[0].setWidth(pos.x - rects[0].attrs.x);
							rects[0].setHeight(rects[0].attrs.height*1 + difY*1);
						}
						else if(currentPointIndex == 2)
						{
							rects[0].setWidth(pos.x - rects[0].attrs.x);
							rects[0].setHeight(pos.y - rects[0].attrs.y);
						}
						else if(currentPointIndex == 3)
						{
							var difX = rects[0].attrs.x - pos.x;
							
							rects[0].setX(pos.x);
							rects[0].setWidth(rects[0].attrs.width*1 + difX*1);
							rects[0].setHeight(pos.y - rects[0].attrs.y);
						}
						
						content['hitboxes'][selected['hitboxes'][0]].x = rects[0].attrs.x - hitboxMargin;
						content['hitboxes'][selected['hitboxes'][0]].y = rects[0].attrs.y - hitboxMargin;
						content['hitboxes'][selected['hitboxes'][0]].width = rects[0].attrs.width;
						content['hitboxes'][selected['hitboxes'][0]].height = rects[0].attrs.height;
						
						refreshProperties('hitboxes');
					}
					else if(polygons.length)
					{//POLYGON
						var currentPointIndex = indexOutParent('hitboxPoints', currentPointIndex);
					
						polygons[0].attrs.points[actualPoint.index] = pos.x;
						polygons[0].attrs.points[actualPoint.index+1] = pos.y;
						
						content['hitboxPoints'][currentPointIndex].x = pos.x - hitboxMargin;
						content['hitboxPoints'][currentPointIndex].y = pos.y - hitboxMargin;
						
						refreshProperties('hitboxPoints');
					}
					else if(circles.length - 5 > 0)//-anchor points
					{//CIRCLE					
						if(currentPointIndex == 0)
						{
							circles[0].setX(pos.x);
							circles[0].setY(pos.y);
						}
						else if(currentPointIndex)
						{
							var difX = Math.abs(circles[0].attrs.x - pos.x);
							var difY = Math.abs(circles[0].attrs.y - pos.y);
						
							circles[0].setRadius(Math.sqrt(Math.pow(difX, 2) + Math.pow(difY, 2)));
						}
							
						content['hitboxes'][selected['hitboxes'][0]].x = circles[0].attrs.x - hitboxMargin;
						content['hitboxes'][selected['hitboxes'][0]].y = circles[0].attrs.y - hitboxMargin;
						content['hitboxes'][selected['hitboxes'][0]].width = circles[0].attrs.radius*2;
						
						refreshProperties('hitboxes');
					}
				}
				
				return pos;
			}
		});
		
		hitboxPointCircle.on('mousedown', function(event) {
			actualPoint = event.targetNode.attrs;
			var currentPointIndex = indexOutParent('hitboxPoints', actualPoint.index / 2);
			
			selectListItem('hitboxPoints', currentPointIndex, 'dontRefresh');
			setLastSelectedList('hitboxPoints');
			
			hitboxLayer.find('.hitboxPointCircle')
			.each(function(hitboxPoint){
				if(actualPoint.index == hitboxPoint.attrs.index)
				{
					hitboxPoint.setFill(externColor);
					hitboxPoint.setStroke(internColor);
				}
				else
				{
					hitboxPoint.setFill(internColor);
					hitboxPoint.setStroke(internColor);
				}
			});
			
			if(	hitboxLayer.find('Line').length)//POLYGON
			{			
				if(event.button == 2)//RIGHT CLIC 
				{
					openOptionMenu('hitboxPoints', currentPointIndex, {x : event.pageX, y : event.pageY});
					actualPoint = false;
				}
			}
		});
		hitboxPointCircle.on('mouseup', function(event) {
			actualPoint = false;
			drawSelectedHitbox();
		});
			
		for(var j=0; j < selected['hitboxPoints'].length; j++)
		{
			if(indexInParent('hitboxPoints', selected['hitboxPoints'][j]) == (i/2))
				hitboxPointCircle.setFill(externColor);
		}
		
		hitboxLayer.add(hitboxPointCircle);
	}
	
	hitboxContainer.draw();
}

//OTHER FUNCTIONS
function checkCollide(rect1, selectionRect)//CHECK IF 2 RECTS COLLIDE
{
	var r1 = rect1;
	var r2 = selectionRect;
	
	if(r2.width < 0)
	{
		r2.x = r2.x + r2.width;
		r2.width = -r2.width;
	}
	if(r2.height < 0)
	{
		r2.y = r2.y + r2.height;
		r2.height = -r2.height;
	}
	
	r1.right = r1.x + r1.width;
	r2.right = r2.x + r2.width;
	r1.bottom = r1.y + r1.height;
	r2.bottom = r2.y + r2.height;

	return !(r2.x > r1.right || 
		r2.right < r1.x || 
		r2.y > r1.bottom ||
		r2.bottom < r1.y);
}
function getMaxSizeInGroup(group)//GET WIDTH AND HEIGHT OF GROUP DEPENDING ON GROUP'S ELEMENTS
{
	var size = { x : 0, y : 0, width : 0, height : 0 };
	
	for(var elementNum = 0; elementNum < content['groupElements'].length; elementNum++)
	{
		if(group.id == content['groupElements'][elementNum].groupId)
		{
			var currentGroupElements = content['groupElements'][elementNum];
			var currentElementId = content['groupElements'][elementNum].elementId;
			var currentElement = content['elements'][getIndexById('elements', currentElementId)];
			
			var elementMaxSize = getMaxSizeInElement(currentElement);
			
			var newMaxX = currentGroupElements.x*1;
			var newMaxY = currentGroupElements.y*1;
			var anchorPxX = (currentGroupElements.anchorX * elementMaxSize.width);
			var anchorPxY = (currentGroupElements.anchorY * elementMaxSize.height);
			
			var points = Array();
			points[0] = { x : 0 					- anchorPxX, y : 0 						- anchorPxY};
			points[1] = { x : elementMaxSize.width	- anchorPxX, y : 0						- anchorPxY};
			points[2] = { x : 0 					- anchorPxX, y : elementMaxSize.height	- anchorPxY};
			points[3] = { x : elementMaxSize.width	- anchorPxX, y : elementMaxSize.height	- anchorPxY};
			
			for(var i = 0; i < points.length; i++)
			{
				var pointRotated = getRotatedPoint(currentGroupElements.rotation, points[i]);
				if(newMaxX + pointRotated.x < size.x)
					size.x = newMaxX + pointRotated.x;
				if(newMaxY + pointRotated.y < size.y)
					size.y = newMaxY + pointRotated.y;
					
				if(newMaxX + pointRotated.x > size.width)
					size.width = newMaxX + pointRotated.x;
				if(newMaxY + pointRotated.y > size.height)
					size.height = newMaxY + pointRotated.y;
			}
		}
	}
	
	size.width -= size.x;
	size.height -= size.y;
	
	return size;
}
function getMaxSizeInElement(element)//GET WIDTH AND HEIGHT OF ELEMENT DEPENDING ON ELEMENTS'S ASSETS AND HITBOXES
{	
	var size = { x : 0, y : 0, width : 0, height : 0 };
	for(var assetNum = 0; assetNum < content['elementAssets'].length; assetNum++)
	{
		if(element.id == content['elementAssets'][assetNum].elementId)
		{
			var currentElementAssets = content['elementAssets'][assetNum];
			var currentAssetId = content['elementAssets'][assetNum].assetId;
			var currentAsset = content['assets'][getIndexById('assets', currentAssetId)];
			
			//var currentWidth = currentElementAssets.x*1 + (currentAsset.width * currentElementAssets.scaleX);
			var realX = currentElementAssets.x*1;
			if(realX < size.x)
				size.x = realX;
			var currentWidth = realX + (currentAsset.width * currentElementAssets.scaleX);
			if(size.width < currentWidth)
				size.width = currentWidth;
				
			var realY = currentElementAssets.y*1;
			if(realY < size.y)
				size.y = realY;
			var currentHeight = realY + (currentAsset.height * currentElementAssets.scaleY);
			if(size.height < currentHeight)
				size.height = currentHeight;
		}
	}
	
	
	for(var hitboxNum = 0; hitboxNum < content['elementHitboxes'].length; hitboxNum++)
	{
		if(element.id == content['elementHitboxes'][hitboxNum].elementId)
		{
			var currentElementHitboxes = content['elementHitboxes'][hitboxNum];
			var currentHitboxId = content['elementHitboxes'][hitboxNum].hitboxId;
			var currentHitbox = content['hitboxes'][getIndexById('hitboxes', currentHitboxId)];
			
			if(currentHitbox.type == 0)
			{
				var currentWidth = currentElementHitboxes.x*1 + currentHitbox.x*1 + (currentHitbox.width * currentElementHitboxes.scaleX);
				
				if(size.width < currentWidth)
					size.width = currentWidth;
					
				var currentHeight = currentElementHitboxes.y*1 + currentHitbox.y*1 + (currentHitbox.height * currentElementHitboxes.scaleY);
				if(size.height < currentHeight)
					size.height = currentHeight;
			}
			else if(currentHitbox.type == 1)
			{
				var currentWidth = currentElementHitboxes.x*1 + currentHitbox.x*1 + ((currentHitbox.width/2) * currentElementHitboxes.scaleX);
				var currentHeight = currentElementHitboxes.y*1 + currentHitbox.y*1 + ((currentHitbox.width/2) * currentElementHitboxes.scaleX);
				
				if(size.width < currentWidth)
					size.width = currentWidth;
				if(size.height < currentHeight)
					size.height = currentHeight;
			
			}
			else if(currentHitbox.type == 2)
			{
				for(var hitboxPointsNum = 0; hitboxPointsNum < content['hitboxPoints'].length; hitboxPointsNum++)
				{
					if(currentHitbox.id == content['hitboxPoints'][hitboxPointsNum].hitboxId)
					{
						var currentHitboxPoints = content['hitboxPoints'][hitboxPointsNum];
						
						var currentWidth = currentElementHitboxes.x*1 + currentHitboxPoints.x*1;
						var currentHeight = currentElementHitboxes.y*1 + currentHitboxPoints.y*1;
						
						if(size.width < currentWidth)
							size.width = currentWidth;
						if(size.height < currentHeight)
							size.height = currentHeight;
					}
				}
			}
		}
	}
	
	size.width -= size.x;
	size.height -= size.y;
	
	return size;
}
function getCurrentHitboxPoints(selectedHitbox, position)//GET HITBOX POINTS (DEPENDS OF HITBOX TYPE)
{
	var currentHitboxPoints = Array();
	if(selected['hitboxes'].length == 1 || selectedHitbox)
	{
		if(!selectedHitbox)
			selectedHitbox = content['hitboxes'][selected['hitboxes'][0]];
		
		if(selectedHitbox.type == 0)//RECTANGLE
		{
			var point0 = new Object();
			point0.x = selectedHitbox.x;
			point0.y = selectedHitbox.y;
			var point1 = new Object();
			point1.x = selectedHitbox.x*1 + selectedHitbox.width*1;
			point1.y = selectedHitbox.y;
			var point2 = new Object();
			point2.x = selectedHitbox.x*1 + selectedHitbox.width*1;
			point2.y = selectedHitbox.y*1 + selectedHitbox.height*1;
			var point3 = new Object();
			point3.x = selectedHitbox.x;
			point3.y = selectedHitbox.y*1 + selectedHitbox.height*1;
			
			currentHitboxPoints[0] = point0;
			currentHitboxPoints[1] = point1;
			currentHitboxPoints[2] = point2;
			currentHitboxPoints[3] = point3;
		}
		else if(selectedHitbox.type == 1)//ROND
		{
			var point0 = new Object();
			point0.x = selectedHitbox.x;
			point0.y = selectedHitbox.y;
			var point1 = new Object();
			point1.x = selectedHitbox.x*1 + selectedHitbox.width/2;
			point1.y = selectedHitbox.y;
			var point2 = new Object();
			point2.x = selectedHitbox.x*1 - selectedHitbox.width/2;
			point2.y = selectedHitbox.y;
			var point3 = new Object();
			point3.x = selectedHitbox.x;
			point3.y = selectedHitbox.y*1 + selectedHitbox.width/2;
			var point4 = new Object();
			point4.x = selectedHitbox.x;
			point4.y = selectedHitbox.y*1 - selectedHitbox.width/2
			
			currentHitboxPoints[0] = point0;
			currentHitboxPoints[1] = point1;
			currentHitboxPoints[2] = point2;
			currentHitboxPoints[3] = point3;
			currentHitboxPoints[4] = point4;
		}
		else if(selectedHitbox.type == 2)//POLYGONE
		{
			for(var i=0; i < content['hitboxPoints'].length; i++)
			{
				if(selectedHitbox.id == content['hitboxPoints'][i]['hitboxId'])
				{
					currentHitboxPoints[currentHitboxPoints.length] = content['hitboxPoints'][i];
				}
			}
		}		
		
		for(var i=0; i < currentHitboxPoints.length; i++)
			currentHitboxPoints[i].midPoint = 'true';
	
	}
	
	var toReturn = new Array();
	for(var i = 0; i < currentHitboxPoints.length; i++)
	{
		toReturn[toReturn.length] = currentHitboxPoints[i].x*1 + position.x*1;
		toReturn[toReturn.length] = currentHitboxPoints[i].y*1 + position.y*1;
	}
	return toReturn;
}
function getRotatedPoint(rotation, point)
{
	var angleRad = rotation * Math.PI / 180.0;
	var cosa = Math.cos(angleRad)
	var sina = Math.sin(angleRad);
	return { x : point.x * cosa - point.y * sina, y : point.x * sina + point.y * cosa};
}
//SELECTION RECT GESTION
$(function(){
	$('#C_levelImage').on('mousedown', '.kineticjs-content', function(event) {
		if(!onLayoutElements)
		{
			selectionStartPoint = { x : event.offsetX, y : event.offsetY };
			selectionRect.setX(selectionStartPoint.x);
			selectionRect.setY(selectionStartPoint.y);
			selectionRect.setWidth(0);
			selectionRect.setHeight(0);
			selectionRect.show();
			levelContainer.draw();
		}
	});
	$('#C_levelImage').on('mousemove', '.kineticjs-content', function(event) {
		if(selectionStartPoint)
		{
			selectionRect.setWidth(event.offsetX - selectionStartPoint.x);
			selectionRect.setHeight(event.offsetY - selectionStartPoint.y);
			levelContainer.draw();
		}
	});
});