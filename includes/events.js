document.oncontextmenu = function(){return false;};

window.onbeforeunload = function(e) {
	if(!saved)
		return 'Des changements n\'ont pas �t�s sauvegard�s';
}

$(function(){
	$(window)
	.load(function(){
		//VERTICAL
		$('.categorie')
		.each(function(){
			$(this).css({'width': minCategoryWidth, 'height': minCategoryHeight});
		});
		
		$('#zoomValue').html(' ('+levelZoom+'%)');
		getDatas();
	})
	.keyup(function(e){
		if(e.keyCode == 46 && content[lastSelectionnedList])
		{
			if(lastSelectionnedList == 'groups' || lastSelectionnedList == 'levels')
			{
				if(confirm('Voulez vous vraiment supprimer ce truc ?'))
					updateList(lastSelectionnedList, 'remove');
			}
			else
			{
				updateList(lastSelectionnedList, 'remove');
			}
		}
	})
	.resize(function(){
		//SET HTML WIDTH TO CATEGORIE'S TOTAL WIDTH
		var maxWidth = 0;
		
		$('.categorie')
		.each(function(){
			maxWidth += $(this).outerWidth();
		});	
		maxWidth += 100;
		
		if(maxWidth < $(window).width())
			maxWidth = $(window).width();
		
		$('body').width(maxWidth);
	
		$('.categorie')
		.each(function(){
			maximizeCategory($(this), true, true);
		});	
	})
	.bind("mousewheel", function(event, delta) {
		if(event.shiftKey)
		{
			if(event.originalEvent.wheelDelta > 0 && levelZoom < 1000)
				levelZoom += 10;
			else if(event.originalEvent.wheelDelta <= 0 && levelZoom > 10)
				levelZoom -= 10;
			
			$('#C_levelImage').attr('width', 768 * (levelZoom/100));
			$('#C_levelImage').attr('height', 1024 * (levelZoom/100));
			
			if(levelZoom != 100)
				$('#zoomValue').html(' ('+levelZoom+'%)');
			else
				$('#zoomValue').html('');
		
			drawSelectedLevel();
			
			maximizeCategory($('#D_levelCreator'));
			
			return false;
		}
	})
	.mouseup(function(e){
		if(dragging['hitboxPoints'].length == 1)
			$('#TB_hitboxPointX').focus();
			
		actualPoint = false;
		currentGraphicGroupGroup = false;
		currentGraphicElementGroup = false;
		if(currentGraphicGroupElementGroup)
		{
			drawSelectedGroup();
			currentGraphicGroupElementGroup = false
		}
		//disabled all drags
		for(var index in dragging)
		{
			dragging[index] = Array();
		}
		
		/*
		if(startPoint != -1)
		{
			startPoint = -1;
			refreshScreen('levels');
			refreshScreen('hitboxes');
		}
		*/
		
		//Right clic
		switch (e.which) {
			case 1:
				closeOptionMenu();
				break;
		}
		
		copied = false;
		
		if(selectionStartPoint)
		{
			selected['layoutElements'] = Array();
			selected['layoutGroups'] = Array();
			
			selectionStartPoint = false;
			//Elements multiple selection
			for(var i = 0; i < elementsLayer.children.length; i++)
			{
				var currentLayer = elementsLayer.children[i];
				for(var j = 0; j < currentLayer.children.length; j++)
				{
					var groupAttrs = currentLayer.children[j].attrs;
					if(!isNaN(groupAttrs.layoutElementsIndex))
					{
						var currentLayoutElement = content['layoutElements'][groupAttrs.layoutElementsIndex];
						var currentElement = content['elements'][getIndexById('elements', currentLayoutElement.elementId)];
						var elementSize = getMaxSizeInElement(currentElement);
						
						var anchorSize = 10 * groupAttrs.scaleX;
						var rectElementSize = { x : groupAttrs.x - (anchorSize/2), y : groupAttrs.y - (anchorSize/2), width : anchorSize, height : anchorSize};
						var selectionRectSize = selectionRect.attrs;

						if(checkCollide(rectElementSize, selectionRectSize))
						{
							if(!groupAttrs.hasSelectionRect)
							{
								var blackSelectionRect = new Kinetic.Rect({
									isSelectionRect : true,
									width: elementSize.width,
									height: elementSize.height,
									stroke: internColor
								});
								currentLayer.children[j].add(blackSelectionRect);
								groupAttrs.hasSelectionRect = true;
							}
							
							selected['layoutElements'][selected['layoutElements'].length] = groupAttrs.layoutElementsIndex;
							
						}
						else
						{
							if(groupAttrs.hasSelectionRect)
							{
								for(var k = 0; k < currentLayer.children[j].children.length; k++)
								{
									if(currentLayer.children[j].children[k].attrs.isSelectionRect)
									{
										currentLayer.children[j].children[k].remove();
									}
								}
								groupAttrs.hasSelectionRect = false;
							}
						}
					}
				}
			}
			//Groups multiple selection
			for(var i = 0; i < elementsLayer.children.length; i++)
			{
				var currentLayer = elementsLayer.children[i];
				for(var j = 0; j < currentLayer.children.length; j++)
				{
					var groupAttrs = currentLayer.children[j].attrs;
					if(!isNaN(groupAttrs.layoutGroupsIndex))
					{
						var currentLayoutGroup = content['layoutGroups'][groupAttrs.layoutGroupsIndex];
						var currentGroup = content['groups'][getIndexById('groups', currentLayoutGroup.groupId)];
						var groupSize = getMaxSizeInElement(currentGroup);
						
						var anchorSize = 10 * groupAttrs.scaleX;
						var rectElementSize = { x : groupAttrs.x - (anchorSize/2), y : groupAttrs.y - (anchorSize/2), width : anchorSize, height : anchorSize};
						var selectionRectSize = selectionRect.attrs;

						if(checkCollide(rectElementSize, selectionRectSize))
						{
							if(!groupAttrs.hasSelectionRect)
							{
								var blackSelectionRect = new Kinetic.Rect({
									isSelectionRect : true,
									width: groupSize.width,
									height: groupSize.height,
									stroke: internColor
								});
								currentLayer.children[j].add(blackSelectionRect);
								groupAttrs.hasSelectionRect = true;
							}
							
							selected['layoutGroups'][selected['layoutGroups'].length] = groupAttrs.layoutGroupsIndex;
							
						}
						else
						{
							if(groupAttrs.hasSelectionRect)
							{
								for(var k = 0; k < currentLayer.children[j].children.length; k++)
								{
									if(currentLayer.children[j].children[k].attrs.isSelectionRect)
									{
										currentLayer.children[j].children[k].remove();
									}
								}
								groupAttrs.hasSelectionRect = false;
							}						
						}					
					}
				}
			}
			
			selectionRect.hide();
			refreshScreen('levels');
		}
		
	});
	
	//GESTION DE LA GRILLE
	$('#CB_grille, #TB_largeurGrille, #TB_hauteurGrille')
	.change(function(){
		if($(this).val() <= 0)
			$(this).val(1);			
		drawSelectedLevel();
	})
	.keyup(function(){
		$(this).change();
	});
	//GESTION DES GROUPES
	$('#CB_grilleGroupe, #TB_largeurGrilleGroupe, #TB_hauteurGrilleGroupe')
	.change(function(){
		if($(this).val() <= 0)
			$(this).val(1);			
		drawSelectedGroup();
	})
	.keyup(function(){
		$(this).change();
	});
	
	$('#CB_pointsAncrage')
	.change(function(){
		if($(this).is(':checked'))
			showAnchor = false;
		else
			showAnchor = true;
		drawSelectedLevel();
	});
	
	$('#CB_couleurFond')
	.change(function(){
		externColor = 'white';
		internColor = 'black';
		if($(this).is(':checked'))
		{
			externColor = 'black';
			internColor = 'white';
		}
		drawSelectedLevel();
	});
	
	//OPEN AND CLOSE BOXES
	$('.titre')
	.click(function(){
		//VERTICAL	
		var maxWidth = 0;
		$(this).parent().children('.fleft, .container')
		.each(function(){
			if($(this).is(':visible'))
				maxWidth += $(this).outerWidth() + 5;
		});
		var maxHeight = $(window).height() - $('header').outerHeight() - 30;// - boxes padding and margin
		
		if($(this).parent().width() == maxWidth && $(this).parent().height() == maxHeight)
		{
			$(this).parent().stop().animate({'width' : minCategoryWidth, 'height' : minCategoryHeight}, 200, function(){
				$(window).resize();
			});
		}
		else
		{
			maximizeCategory($(this).parent());
		}
	});
	
	//RIGHT CLIC MENU
	$('#removeListItemOptionMenu')
	.click(function(){
		if(!isNaN(rightClicListItemIdx))
		{
			var currentList = getListByClasses($('.list div').eq(rightClicListItemIdx).attr('class'));
			if(currentList)
				updateList(currentList, 'remove');
		}
	});
	$('#addListItemOptionMenu')
	.click(function(){
		if(!isNaN(rightClicListItemIdx))
		{
			var currentList = getListByClasses($('.list div').eq(rightClicListItemIdx).attr('class'));
			if(currentList)
			{
				var defaultDatas = getDefaultDatasToAdd(currentList);
				updateList(currentList, 'add', defaultDatas);
			}
		}
	});
	$('#removePointOptionMenu')
	.click(function(){
		updateList('hitboxPoints', 'remove');
	});
	$('#addPointOptionMenu')
	.click(function(){
		var defaultDatas = getDefaultDatasToAdd('hitboxPoints');
		updateList('hitboxPoints', 'add', defaultDatas);
	});
	$('#removeLayoutElementOptionMenu')
	.click(function(){
		updateList('layoutElements', 'remove');
	});
	$('#addLayoutElementOptionMenu')
	.click(function(){
		var defaultDatas = getDefaultDatasToAdd('layoutElements');
		updateList('layoutElements', 'add', defaultDatas);
	});
	$('#removeLayoutGroupOptionMenu')
	.click(function(){
		updateList('layoutGroups', 'remove');
	});
	$('#addLayoutGroupOptionMenu')
	.click(function(){
		var defaultDatas = getDefaultDatasToAdd('layoutGroups');
		updateList('layoutGroups', 'add', defaultDatas);
	});
	
	//UPLOAD ELEMENTS IMAGES
	$('#F_changeAssetImage').ajaxForm({
		beforeSend: function() {
			loading(true);
			
			$('#D_statusText').html('0%');
			$('#D_colorBar').css('width', '0');
				
		},
		uploadProgress: function(event, position, total, percentComplete) {
			var percentVal = percentComplete + '%';
			$('#D_statusText').html(percentVal);
			$('#D_colorBar').css('width', percentVal);
		},
		success: function(data) {
			loading(false);
			
			$('#D_statusText').html('Image bien envoy�e');
			$('#D_colorBar').css('width', '0');
			
			if($('.D_layoutAssets.selected').length)
				$('.D_layoutAssets.selected:first').click();
			
			getDatas(true)
		}
	});  
	
	//ADD BUTTONS
	$('.I_add')
	.click(function(){
		var currentList = getListByClasses($(this).attr('class'));
		if(currentList)
		{
			var defaultDatas = getDefaultDatasToAdd(currentList);
			updateList(currentList, 'add', defaultDatas);
		}		
	});
	
	//DELETE BUTTONS
	$('.I_remove')
	.click(function(){
		var currentList = getListByClasses($(this).attr('class'));		
		if(currentList)
			updateList(currentList, 'remove');
	});
	
	$('#TB_levelLayouts_orderPlus, #TB_levelLayouts_orderMinus')
	.click(function(){
		var lastBeforeSelected = -1;
		var nextAfterSelected = -1;
		
		for(var i=0; i < content['levelLayouts'].length; i++)
		{
			if(i != selected['levelLayouts'][0]
				&& dependencies['levelLayouts']
				&& selected[dependencies['levelLayouts']].length 
				&& content['levelLayouts'][i][getParentField('levelLayouts')] == content[dependencies['levelLayouts']][selected[dependencies['levelLayouts']][0]].id)
			{
				if(i < selected['levelLayouts'][0])
					lastBeforeSelected = i;
				else
				{
					nextAfterSelected = i;
					break;
				}
			}
		}
		
		var otherIndex = nextAfterSelected;
		var toAddIfEqual = 1;
		if($(this).attr('id') == 'TB_levelLayouts_orderPlus')
		{
			var otherIndex = lastBeforeSelected;
			toAddIfEqual = -1;
		}
		
		if(otherIndex != -1)
		{	
			var temp = content['levelLayouts'][selected['levelLayouts'][0]].weight;
			
			if(content['levelLayouts'][otherIndex].weight == temp)
				temp = content['levelLayouts'][otherIndex].weight*1 + toAddIfEqual*1;
				
			content['levelLayouts'][selected['levelLayouts'][0]].weight = content['levelLayouts'][otherIndex].weight;
			content['levelLayouts'][otherIndex].weight = temp; 
			
			var contentTemp = content['levelLayouts'][selected['levelLayouts'][0]];
			content['levelLayouts'][selected['levelLayouts'][0]] = content['levelLayouts'][otherIndex];
			content['levelLayouts'][otherIndex] = contentTemp; 
					
			refreshList('levelLayouts');
			selectListItem('levelLayouts', otherIndex);	
		}
	});
	
	//PROPERTIES FIELDS
	$('.TB_editProperty')
	.change(function(){
		var idParts = $(this).attr('id').split('_');
		if(idParts.length == 3 && content[idParts[1]])
		{
			var currentList = idParts[1];
			var propertyName = idParts[2];

			if(selected[currentList].length == 1)
			{
			
				if(currentList == 'layoutElements')
				{
					if($(this).val() < 10 && (propertyName == 'width' || propertyName == 'width'))
						$(this).val(10);
					
					if($('#CB_proportionsLayoutElements').is(':checked') && content[currentList][selected[currentList]][propertyName] != 0)
					{
						if(propertyName == 'width')
						{						
							content[currentList][selected[currentList]]['height'] *= ($(this).val() / content[currentList][selected[currentList]][propertyName]);						
							content[currentList][selected[currentList]]['height'] = Math.round(content[currentList][selected[currentList]]['height']);
						}
						else if(propertyName == 'height')
						{						
							content[currentList][selected[currentList]]['width'] *= ($(this).val() / content[currentList][selected[currentList]][propertyName]);						
							content[currentList][selected[currentList]]['width'] = Math.round(content[currentList][selected[currentList]]['width']);
						}
					}
				}
				
				if($(this).attr('type') == 'checkbox')
				{
					if($(this).is(':checked'))
					{
						if(propertyName == 'export')
						{
							for(var i = 0; i < content[currentList].length; i++)
								content[currentList][i][propertyName] = 0;
						}
						
						content[currentList][selected[currentList]][propertyName] = 1;
					}
					else
					{
						content[currentList][selected[currentList]][propertyName] = 0;
					}
				}
				else
				{
					if(currentList == 'hitboxes' && propertyName == 'type')
					{
						if($(this).val() == 0)
						{
							$('#D_hitboxes_height').show();
							$('#D_hitboxes_properties').show();
							$('#D_hitboxPointsListModule').hide();
						}
						else if($(this).val() == 1)
						{
							$('#D_hitboxes_height').hide();
							$('#D_hitboxes_properties').show();
							$('#D_hitboxPointsListModule').hide();
						}
						else if($(this).val() == 2)
						{
							$('#D_hitboxes_properties').hide();
							$('#D_hitboxPointsListModule').show();
						}
						
						maximizeCategory($('#D_hitboxCreator'));
						
					}
					
					if(content[currentList][selected[currentList]][propertyName] != $(this).val())
						saved = false;
						
					content[currentList][selected[currentList]][propertyName] = $(this).val();					
				}
				
				if(propertyName == 'name')
					refreshList(currentList);
				
				if(!isLoadingElement)
					refreshScreen(currentList);
			}			
		}	
	})
	.keydown(function(e){
		if(e.keyCode >= 37 && e.keyCode<=40)
		{
			var idParts = $(this).attr('id').split('_');
			if(idParts.length == 3 && content[idParts[1]])
			{
				var currentList = idParts[1];
				var propertyName = idParts[2];
					
				if(propertyName == 'x' || propertyName == 'y')
				{
					var xElement = $('#TB_'+currentList+'_x');
					var yElement = $('#TB_'+currentList+'_y');
				}
				else if(propertyName == 'width' || propertyName == 'height')
				{
					var xElement = $('#TB_'+currentList+'_width');
					var yElement = $('#TB_'+currentList+'_height');
				}
				
				if(xElement && yElement)
				{
					if(e.keyCode==37)
						if(!(xElement.attr('min')) || xElement.attr('min') <= xElement.val()-1)
							xElement.val(xElement.val()*1-1);
					
					if(e.keyCode==38)
						if(!(yElement.attr('min')) || yElement.attr('min') <= yElement.val()-1)
							yElement.val(yElement.val()*1-1);
						
					if(e.keyCode==39)
						xElement.val(xElement.val()*1+1);
						
					if(e.keyCode==40)
						yElement.val(yElement.val()*1+1);
						
					$(this).change();
					return false;
				}
			}
		
		}
	})
	.keyup(function(e){
		$(this).change();
	});

	//CLIC ON LAYER CHECKBOX
	$(document).on('change', '.D_levelLayouts input', function(){
		refreshScreen('levels');
	});
	
	//CLIC ON LIST ITEM
	$('.list').on('mousedown', 'div', function(e){
		var classe = $(this).attr("class");
		var classList = classe.split(' ');	
		for(var i = 0; i < classList.length; i++)
		{
			var classTest = classList[i].split('D_');		
			if(classTest.length == 2)
			{
				var currentList = classTest[1];
				
				setLastSelectedList(currentList);
		
				selectListItem(currentList, indexOutParent(currentList, $(this).index()));
				
				if(e.button == 2)
				{
					rightClicListItemIdx = indexOutParent(currentList, $('.list div').index(this));
					openOptionMenu('list', 0, {x : e.pageX, y : e.pageY});
				}
					
				break;
			}
		}
	});

	$('.navigate-arrow')
	.click(function(){
		if($(this).index() == 0)
		{
			$('html, body').animate({scrollLeft: 0}, 200);
		}
		else
		{
			$('html, body').animate({scrollLeft: $('html').width()}, 200);
		}
	});
	
	//SAVE
	$('#B_save')
	.click(function(){
		getDatas(true);
		return false;
	});
});