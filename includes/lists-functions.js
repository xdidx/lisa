function updateList(whichList, operation, datas, save)//ADD, UPDATE OR REMOVE DATAS
{
	if(operation == 'add')
	{			
		var newIdx = content[whichList].length;
		content[whichList][newIdx] = new Object();
		
		for(var index in datas)
			content[whichList][newIdx][index] = datas[index];
		
		if(!save)
			getDatas('save');
	}
	else if(operation == 'remove')
	{
		var alreadyRemoved = Array();
			
		for(var i=0; i < selected[whichList].length; i++)
		{
			var curIdx = selected[whichList][i];			
			alreadyRemoved[alreadyRemoved.length] = curIdx;
			
			var toSubstract = 0;
			for(var j=0; j < alreadyRemoved.length; j++)
			{
				if(alreadyRemoved[j] < curIdx)
					toSubstract++;
			}
			curIdx -= toSubstract;
			
			content[whichList].splice(curIdx, 1);
		}
		
		selected[whichList] = Array();
	}
	else if(operation == 'update')
	{
		if(selected[whichList].length == 1)
			for(var index in datas)
				current[whichList][selected[whichList][0]][index] = datas[index];
	}

	if(operation != 'add')
	{
		saved = false;
		refreshList(whichList);
		refreshScreen(whichList);
	}
}
function refreshList(whichList)//REFRESH ITEMS' LIST
{
	var itemsNumber	= 0;
	
	if(content[whichList] && content[whichList].length)
	{
		$('#D_'+whichList+'List').html('');
		if(whichList == 'groups')
			$('#S_newGroupsList').html('');
			
		if(whichList == 'elements')
		{
			$('#S_newElementsList').html('');			
			$('#S_newGroupElementsList').html('');			

			var newOption = document.createElement('option');
			$(newOption).val(0);
			$(newOption).html('Aucun');			
			$('#TB_hitboxes_modeleId').html(newOption);
		}
		
		if(whichList == 'hitboxes')
			$('#S_newElementHitboxesList').html('');
		if(whichList == 'assets')
			$('#S_newElementAssetsList').html('');
			
		if(whichList == 'levelLayouts')
		{
			$('#TB_layoutElements_layoutId').html('');
			$('#TB_layoutGroups_layoutId').html('');
		}
		
		for(var i=0; i < content[whichList].length; i++)
		{
			if(getParentField(whichList) == '' 
				|| (dependencies[whichList]
					&& selected[dependencies[whichList]].length 
					&& content[whichList][i][getParentField(whichList)] == content[dependencies[whichList]][selected[dependencies[whichList]][0]].id))
			{
				var newDiv = document.createElement('div');
				//$(newDiv).attr('id', content[whichList][i].id)
				$(newDiv).addClass('D_'+whichList);
				$(newDiv).html(content[whichList][i].name);
				
				if(whichList == 'levelLayouts')
					$(newDiv).prepend('<input type="checkbox" checked="checked" />');
				
				$('#D_'+whichList+'List').append(newDiv);	
				
				if(whichList == 'groups')
				{				
					var newOption = document.createElement('option');
					$(newOption).val(content[whichList][i].id);
					$(newOption).html(content[whichList][i].name);
					$('#S_newGroupsList').append(newOption);
				}
				
				if(whichList == 'elements')
				{
					var newOption = document.createElement('option');
					$(newOption).val(content[whichList][i].id);
					$(newOption).html(content[whichList][i].name);
					$('#S_newElementsList').append(newOption);
					
					var newOption = document.createElement('option');
					$(newOption).val(content[whichList][i].id);
					$(newOption).html(content[whichList][i].name);
					$('#S_newGroupElementsList').append(newOption);
				}
				
				if(whichList == 'assets')
				{
					var newOption = document.createElement('option');
					$(newOption).val(content[whichList][i].id);
					$(newOption).html(content[whichList][i].name);
					$('#S_newElementAssetsList').append(newOption);	
					
					var newOption = document.createElement('option');
					$(newOption).val(content[whichList][i].id);
					$(newOption).html(content[whichList][i].name);
					$('#TB_hitboxes_modeleId').append(newOption);			
				}
				if(whichList == 'hitboxes')
				{
					var newOption = document.createElement('option');
					$(newOption).val(content[whichList][i].id);
					$(newOption).html(content[whichList][i].name);
					$('#S_newElementHitboxesList').append(newOption);
				}
						
				if(whichList == 'levelLayouts')
				{	
					var newOption = document.createElement('option');
					$(newOption).val(content[whichList][i].id);
					$(newOption).html(content[whichList][i].name);
					$('#TB_layoutElements_layoutId').append(newOption);
					
					var newOption = document.createElement('option');
					$(newOption).val(content[whichList][i].id);
					$(newOption).html(content[whichList][i].name);
					$('#TB_layoutGroups_layoutId').append(newOption);
				}
				
				itemsNumber++;
			}
		}
	}
	
	for(var index in dependencies)
	{
		if(dependencies[index] == whichList)
		{
			selected[index] = Array();
			refreshList(index);
		}
	}
	
	/*
	if(whichList == 'levels' || whichList == 'levelLayouts' || whichList == 'layoutElements' || whichList == 'layoutGroups')
	{
		refreshScreen('levels');
	}
	
	if(whichList == 'elements')
	{
		refreshScreen('levels');
		refreshScreen('elements');
	}
		
	if(whichList == 'hitboxes' || whichList == 'hitboxPoints')
	{
		refreshScreen('hitboxes');
		refreshScreen('elements');
	}
		
	if(whichList == 'assets' || whichList == 'hitboxes' || whichList == 'hitboxPoints' || whichList == 'elementHitboxes' || whichList == 'elementAssets')
	{
		refreshScreen('elements');
	}	
	
	if(whichList == 'groups' || whichList == 'groupElements')
	{
		refreshScreen('groups');
	}
	*/
	
	if(itemsNumber == 0)
	{
		$('#D_'+whichList+'List').html('Aucun item');
	}
	else if(selected[whichList].length == 0)
	{
		selectListItem(whichList, 0);
	}	
}
function refreshScreen(whichList, dontDraw)//REFRESH CANVAS AND SELECTED ITEMS PROPERTIES
{
	var starRegex = new RegExp("[\*]", "g");
	
	if(saved)
	{
		window.document.title = window.document.title.replace(starRegex, '');
	}
	else if(!window.document.title.match(starRegex))
	{
		window.document.title += '*';
	}
	
	if(whichList == 'levels')
		refreshProperties('layoutElements');
	
	refreshProperties(whichList);
	
	if(!dontDraw)
	{
		if((whichList == 'levels' || whichList == 'levelLayouts' || whichList == 'layoutElements' || whichList == 'layoutGroups')
			&& !isLoadingElement)
			drawSelectedLevel();
		
		if(whichList == 'groups' || whichList == 'groupElements')
			drawSelectedGroup();
		
		if(whichList == 'elements')
			drawSelectedElement();
			
		if((whichList == 'hitboxes' || whichList == 'hitboxPoints') && selected['hitboxes'].length)
		{
			drawSelectedElement();
			drawSelectedHitbox();
		}
		
		if(whichList == 'assets' && selected['assets'].length)
		{
			drawSelectedAsset();
		}
	}
	
	loading(false);
}
function refreshProperties(whichList)//REFRESH PROPERTIES (INPUTS)
{
	if(selected[whichList] && selected[whichList].length == 1)
	{
		var selectedElementContent = content[whichList][selected[whichList][0]];
		
		if(whichList == 'assets')
			$('#H_selectedAssetsId').val(selectedElementContent.id);
		
		for(var index in selectedElementContent)
		{
			var currentInput = $('#TB_'+whichList+'_'+index);
			
			if(currentInput.attr('type') == 'checkbox')
			{
				if(index == 'actionloop')
				{
					if(parseInt(selectedElementContent[index]))
						$('#TB_'+whichList+'_actionbeforeactive').css('display', 'none');
					else
						$('#TB_'+whichList+'_actionbeforeactive').css('display', 'inline');
				}
					
				$('#TB_'+whichList+'_'+index).prop('checked', parseInt(selectedElementContent[index]));
			}
			else if(currentInput.length && currentInput.val() != selectedElementContent[index])
			{
				currentInput.val(selectedElementContent[index]);
			}
		}
	}
}
function selectListItem(whichList, selectedIndex, dontDraw, selectMultiple)//SELECT AN ITEM FROM A LIST
{	
	selected[whichList] = Array();
	
	if(selectedIndex >= 0 && (selectedIndex < content[whichList].length || whichList == 'hitboxPoints'))
	{
		
		isLoadingElement = true;
		
		var haveDependencies = false;
		
		if(selectMultiple)
		{
			var addToSelected = true;
			for(var i = 0; i < selected[whichList].length; i++)
			{
				if(selectedIndex == selected[whichList][i])
				{
					addToSelected = false
					break;
				}
			}
			
			if(addToSelected)
				selected[whichList][selected[whichList].length] = selectedIndex;
		}
		else
		{
			selected[whichList] = Array();		
			selected[whichList][0] = selectedIndex;
			
			$('.D_'+whichList).removeClass('selected');
			$('.D_'+whichList).eq(indexInParent(whichList, selectedIndex)).addClass('selected');				
			
			//Select children
			for(var index in dependencies)
			{
				if(dependencies[index] == whichList && content[index] && content[index].length)
				{
					haveDependencies = true;
					selected[index] = Array();
					refreshList(index);

					for(var i = 0; i < content[index].length; i++)
					{
						if(content[index][i][getParentField(index)] == content[whichList][selectedIndex].id)
						{								
							selectListItem(index, i, true);
							break;
						}
					}
				}
			}
			
		}
		
		refreshScreen(whichList, dontDraw);

		if(whichList == 'hitboxes')//Show or hide modules and properties of hitboxes by type
			$('#TB_hitboxes_type').change();
			
		isLoadingElement = false;
	}
}
function indexInParent(whichList, index)//GET INDEX IN PARENT
{
	var dependency = dependencies[whichList];
	var parentField = getParentField(whichList);
	if(dependency && parentField != '' && selected[dependency].length == 1)
	{
		var selectedParent = content[dependency][selected[dependency][0]];
		for(var i=0; i < content[whichList].length; i++)
		{
			if(selectedParent.id == content[whichList][i][parentField])
				break;
			else
				index--;
		}
	}
	return index;
}
function indexOutParent(whichList, index)//GET GLOBAL INDEX
{
	var tempIndex = 0;
	var dependency = dependencies[whichList];
	var parentField = getParentField(whichList);
	if(dependency && parentField != '' && selected[dependency].length == 1)
	{
		var selectedParent = content[dependency][selected[dependency][0]];
		for(var i=0; i < content[whichList].length; i++)
		{				
			if(selectedParent.id == content[whichList][i][parentField])
				tempIndex++;
			
			if((tempIndex-1) == index)
				return i;		
		}
	}
	
	return index;
}
function getParentField(whichList)//GET DB PARENT FIELD
{
	if(parentField[whichList])
		return parentField[whichList];
	else 
		return '';
}
function getIndexById(whichList, currentId)//GET GLOBAL INDEX OF LIST ITEM BY ID
{
	/*
	//Que les �l�ments affich�s
	$('.D_'+whichList)
	.each(function(){
		if(content[whichList][$(this).index()].id == currentId)
		{
			index = $(this).index();
			return false;
		}
	});
	*/
	for(var i = 0; i < content[whichList].length; i++)
	{
		if(content[whichList][i].id == currentId)
		{
			return i;
		}
	}
	
	return -1;
}
function getListByClasses(classes)//GET LIST NAME BY BOX CLASSES (HTML)
{
	var classe = classes.split(' ');
	for(var i = 0; i < classe.length; i++)
	{
		var classParts = classe[i].split('_');
		if(classParts.length == 2 && content[classParts[1]])
			return classParts[1];		
	}
	
	return false;
}