<?php
include 'init.php';

if(isset($_FILES['imageUpload']) && isset($_POST['selectedAssetsId']) && is_numeric($_POST['selectedAssetsId']) && $_POST['selectedAssetsId'] != 0)
{
	$tmp = $_FILES["imageUpload"]["tmp_name"];
	$src = basename($_FILES["imageUpload"]["name"]);
	$name = str_replace('-', ' ', basename($_FILES["imageUpload"]["name"], '.png'));
	
	$image_info = getimagesize($tmp);
	$queryElement = $bdd->prepare('UPDATE asset SET src = ?, name = ?, width = ?, height = ? WHERE id = ?') or die(mysql_error());
	$queryElement->execute(array($src, $name, $image_info[0], $image_info[1], $_POST['selectedAssetsId']));
	
	$location = '../images/assets/'.$src;
	move_uploaded_file($tmp, $location);
}
?>