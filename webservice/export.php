<?php
include './init.php';

$JsonObject = array();
$JsonObject['bodies'] = array();
$JsonObject['skins'] = array();
$JsonObject['game-objects'] = array();
$JsonObject['groups'] = array();
$JsonObject['levels'] = array();

$datasToExport['groupElementsAnimation'] = array('actionpositionx' => 'x', 'actionpositiony' => 'y', 'actionrotation' => 'rotation', 'actiontime' => 'time', 'actionloop' => 'loop', 'actionreverse' => 'reverse', 'actionbeforereverse' => 'beforereverse', 'actionbeforeactive' => 'beforeactive');
$datasToExport['layoutElementsAnimation'] = $datasToExport['groupElementsAnimation'];
$datasToExport['layoutGroupsAnimation'] = $datasToExport['groupElementsAnimation'];

$datasToExport['groupElements'] = array('elementId' => 'game-object', 'x' => 'x', 'y' => 'y', 'scaleX' => 'scaleX', 'scaleY' => 'scaleY', 'anchorX' => 'anchorX', 'anchorY' => 'anchorY', 'rotation' => 'rotation');
$datasToExport['layoutElements'] = array('x' => 'x', 'y' => 'y', 'scaleX' => 'scaleX', 'scaleY' => 'scaleY', 'anchorX' => 'anchorX', 'anchorY' => 'anchorY', 'rotation' => 'rotation');
$datasToExport['layoutGroups'] = $datasToExport['layoutElements'];
$datasToExport['bodies'] = $datasToExport['layoutElements'];
$datasToExport['assets'] = array('src' => 'name', 'width' => 'width', 'height' => 'height');
$datasToExport['game-objects-asset'] = array('asset' => 'skin', 'x' => 'x', 'y' => 'y', 'scaleX' => 'scaleX', 'scaleY' => 'scaleY');
$datasToExport['game-objects-hitbox'] = array('hitbox' => 'body', 'x' => 'x', 'y' => 'y');

$datasToExport['hitboxRect'] = array('x' => 'x', 'y' => 'y', 'width' => 'width', 'height' => 'height', 'type' => 'type');
$datasToExport['hitboxCircle'] = array('x' => 'x', 'y' => 'y', 'radius' => 'radius', 'type' => 'type');
$datasToExport['collisionsProperties'] = array('restitution' => 'restitution', 'sensor' => 'sensor');

$idToIndex = array();
$idToIndex['game-objects'] = array();
$idToIndex['bodies'] = array();
$idToIndex['assets'] = array();

$save = array();

//PARSE TO NUMERIC AND DELETE DEFAULT DATAS
function parseToNumeric($dbValue, $columnName = '')
{
	if(is_numeric($dbValue))
	{
		if(is_int($dbValue))
			$dbValue = intval($dbValue);
		else
			$dbValue = floatval($dbValue);
		
		if($columnName == 'rotation')
			$dbValue = -$dbValue;
			
		if(in_array($columnName, array('scaleX', 'scaleY', 'actionloop', 'actionreverse')))
			$defaultValue = 1;
		else if(in_array($columnName, array('anchorX', 'anchorY')))
			$defaultValue = 0.5;
		else if(in_array($columnName, array('restitution')))
			$defaultValue = 0.9;
		else if(in_array($columnName, array('collectible', 'levelsucces')))
			$defaultValue = -1;
		else if(in_array($columnName, array('x', 'y', 'rotation', 'sensor', 'damage', 'breakable', 'actionrotation', 'actionpositiony', 'actionpositionx', 'launcher')))
			$defaultValue = 0;
		else
			return $dbValue;
			
		if($dbValue == $defaultValue)
			return NULL;
	}
	else if($columnName == 'src')
		return basename($dbValue, ".png");
	
	return $dbValue;
}
function getGroupSize($index, $save)
{
	global $JsonObject;
	
	$size = array('x' => 0, 'y' => 0, 'width' => 0, 'height' => 0);
	
	$size['minAnchorX'] = 10000;
	$size['minAnchorY'] = 10000;
	
	$size['maxAnchorX'] = -10000;
	$size['maxAnchorY'] = -10000;
	
	for($i = 0; $i < count($JsonObject['groups'][$index]); $i++)
	{	
		$anchorX = isset($JsonObject['groups'][$index][$i]['anchorX']) ? $JsonObject['groups'][$index][$i]['anchorX'] : 0;
		$anchorY = isset($JsonObject['groups'][$index][$i]['anchorY']) ? $JsonObject['groups'][$index][$i]['anchorY'] : 0;
		
		$elementSize = getElementSize($JsonObject['groups'][$index][$i]['game-object']);
		
		$newX = isset($JsonObject['groups'][$index][$i]['x']) ? $JsonObject['groups'][$index][$i]['x'] : 0;
		
		if( isset( $save['groups'][$index][$i]['y'] ) )
		{
			$newY = $save['groups'][$index][$i]['y'];
			$newX = $save['groups'][$index][$i]['x'];
		}
		else if( isset( $JsonObject['groups'][$index][$i]['y'] ) )
			$newY = $JsonObject['groups'][$index][$i]['y'];
		else
			$newY = 0;
		
		if($size['minAnchorX'] > $newX)
			$size['minAnchorX'] = $newX;
		if($size['minAnchorY'] > $newY)
			$size['minAnchorY'] = $newY;
			
		if($size['maxAnchorX'] < $newX)
			$size['maxAnchorX'] = $newX;
		if($size['maxAnchorY'] < $newY)
			$size['maxAnchorY'] = $newY;
	
	
		$rotation = isset($JsonObject['groups'][$index][$i]['rotation']) ? -$JsonObject['groups'][$index][$i]['rotation'] : 0;
		
		$points = array();
		$points[0] = array('x' => 0						- $anchorX, 'y' => 0						- ($elementSize['height'] - $anchorY));
		$points[1] = array('x' => $elementSize['width']	- $anchorX, 'y' => 0						- ($elementSize['height'] - $anchorY));
		$points[2] = array('x' => 0						- $anchorX, 'y' => $elementSize['height']	- ($elementSize['height'] - $anchorY));
		$points[3] = array('x' => $elementSize['width']	- $anchorX, 'y' => $elementSize['height']	- ($elementSize['height'] - $anchorY));
		
		
		for($j = 0; $j < count($points); $j++)
		{
			$pointRotated = getRotatedPoint($rotation, $points[$j]);
			
			if($newX + $pointRotated['x'] < $size['x'])
				$size['x'] = $newX + $pointRotated['x'];
				
			if($newY + $pointRotated['y'] < $size['y'])
				$size['y'] = $newY + $pointRotated['y'];
			
			
			if($newX + $pointRotated['x'] > $size['width'])
				$size['width'] = $newX + $pointRotated['x'];
				
			if($newY + $pointRotated['y'] > $size['height'])
				$size['height'] = $newY + $pointRotated['y'];
		}		
	}
	
	foreach($size as $key => $val)
		if(is_numeric($val))
			$size[$key] = round($val);
	
	return $size;
}
function pre($objet)
{
    echo"<pre>";
    print_r($objet);
    echo"</pre>";
}
function getElementSize($index)
{
	global $JsonObject;
	$max = array('width' => 0, 'height' => 0);
	
	//SKINS
	if(isset($JsonObject['game-objects'][$index][3]))
	{
		for($i = 0; $i < count($JsonObject['game-objects'][$index][3]); $i++)
		{
			$gameObjectAsset = $JsonObject['game-objects'][$index][3][$i];
			
			$assetIndex = $gameObjectAsset['skin'];
			$assetX = isset($gameObjectAsset['x']) ? $gameObjectAsset['x'] : 0;
			$assetY = isset($gameObjectAsset['y']) ? $gameObjectAsset['y'] : 0;
			$assetWidth = $JsonObject['skins'][$assetIndex]['width'];
			$assetHeight = $JsonObject['skins'][$assetIndex]['height'];
			
			$newTotalY = $assetHeight + $assetY;
			if($newTotalY > $max['height'])
				$max['height'] = $newTotalY;
				
			$newTotalX = $assetWidth + $assetX;
			if($newTotalX > $max['width'])
				$max['width'] = $newTotalX;
		}
	}
	
	//BODIES
	if(isset($JsonObject['game-objects'][$index][2]))
	{
		for($i = 0; $i < count($JsonObject['game-objects'][$index][2]); $i++)
		{
			$gameObjectHitbox = $JsonObject['game-objects'][$index][2][$i];
			
			$hitboxIndex = $gameObjectHitbox['body'];
			$hitboxY = isset($gameObjectHitbox['y']) ? $gameObjectHitbox['y'] : 0;
			$hitboxX = isset($gameObjectHitbox['x']) ? $gameObjectHitbox['x'] : 0;
			
			if($JsonObject['bodies'][$hitboxIndex]['type'] == 0)
			{
				$newTotalX = $hitboxX + $JsonObject['bodies'][$hitboxIndex]['width'];
				if($newTotalX > $max['width'])
					$max['width'] = $newTotalX;
					
				$newTotalY = $hitboxY + $JsonObject['bodies'][$hitboxIndex]['height'];			
				if($newTotalY > $max['height'])
					$max['height'] = $newTotalY;
			}
			else if($JsonObject['bodies'][$hitboxIndex]['type'] == 1)
			{
				$newTotalX = $hitboxX + ($JsonObject['bodies'][$hitboxIndex]['radius']/2);			
				if($newTotalX > $max['width'])
					$max['width'] = $newTotalX;
					
				$newTotalY = $hitboxY + ($JsonObject['bodies'][$hitboxIndex]['radius']/2);			
				if($newTotalY > $max['height'])
					$max['height'] = $newTotalY;
			}
			else if($JsonObject['bodies'][$hitboxIndex]['type'] == 2)
			{
				for($hitboxPointsNum = 0; $hitboxPointsNum < count($JsonObject['bodies'][$hitboxIndex]['points']); $hitboxPointsNum++)
				{
					$newTotalX = $hitboxX + $JsonObject['bodies'][$hitboxIndex]['points'][$i]['x'];				
					if($newTotalX > $max['width'])
						$max['width'] = $newTotalX;
						
					$newTotalY = $hitboxY + $JsonObject['bodies'][$hitboxIndex]['points'][$i]['y'];				
					if($newTotalY > $max['height'])
						$max['height'] = $newTotalY;					
				}
			}		
		}
	}
	
	return $max;
}
function getRotatedPoint($rotation, $point)
{
	$angleRad = ($rotation / 360) * (2.0 * pi());
	$cosa = cos($angleRad);
	$sina = sin($angleRad);
	return array('x' => $point['x'] * $cosa - $point['y'] * $sina, 'y' => $point['x'] * $sina + $point['y'] * $cosa);
}
function exportAnimation($elementInfos, &$exportPart)
{
	if($elementInfos['actionactive'])
	{
		$exportPart['animation'] = array();
		global $datasToExport;
		foreach($datasToExport['layoutElementsAnimation'] as $key => $val)
		{
			if($key == 'actionpositiony')
				$elementInfos[$key] = -$elementInfos[$key];
			
			$dbValue = parseToNumeric($elementInfos[$key], $key);
			if(!is_null($dbValue))
				$exportPart['animation'][$val] = $dbValue;
		}
	}

}

//ASSETS
$queryAssets = $bdd->prepare('SELECT * FROM asset ORDER BY id DESC') or die(mysql_error());
$queryAssets->execute();
for($nb_asset = 0; $datasAssets = $queryAssets->fetch(); $nb_asset++)
{
	$idToIndex['assets'][$datasAssets['id']] = $nb_asset;
	
	$JsonObject['skins'][$nb_asset] = array();
	foreach($datasToExport['assets'] as $key => $val)
	{
		$dbValue = parseToNumeric($datasAssets[$key], $key);
				
		if(!is_null($dbValue))
			$JsonObject['skins'][$nb_asset][$val] = $dbValue;
	}
}

//HITBOXES
$queryHitboxes = $bdd->prepare('SELECT * FROM hitbox ORDER BY id DESC') or die(mysql_error());
$queryHitboxes->execute();
for($bodyNum = 0; $datasHitboxes = $queryHitboxes->fetch(); $bodyNum++)
{
	$idToIndex['bodies'][$datasHitboxes['id']] = $bodyNum;
	
	$newBodyIdx = count($JsonObject['bodies']);
	$JsonObject['bodies'][$newBodyIdx] = array();
	
	if($datasHitboxes['type'] == 0)//RECTANGLE
	{
		$datasHitboxes['width'] /= 2;
		$datasHitboxes['height'] /= 2;

		foreach($datasToExport['hitboxRect'] as $key => $val)
		{
			if($key == 'x')
				$dbValue = $datasHitboxes['x'] + $datasHitboxes['width'];
			else if($key == 'y')
				$dbValue = $datasHitboxes['y'] + $datasHitboxes['height'];
			else
				$dbValue = $datasHitboxes[$key];
			
			$dbValue = parseToNumeric($dbValue);
			
			if(!is_null($dbValue))
				$JsonObject['bodies'][$newBodyIdx][$val] = $dbValue;
		}
	}
	else if($datasHitboxes['type'] == 1)//CIRCLE
	{	
		//CircleBody		
		foreach($datasToExport['hitboxCircle'] as $key => $val)
		{
			if($key == 'radius')
				$dbValue = $datasHitboxes['width'] / 2;
			else
				$dbValue = $datasHitboxes[$key];
			
			$dbValue = parseToNumeric($dbValue);
		
			if(!is_null($dbValue))
				$JsonObject['bodies'][$newBodyIdx][$val] = $dbValue;
		}
	}
	else if($datasHitboxes['type'] == 2)//POLYGONES
	{
		$points = array();
		
		$queryHitboxesPoints = $bdd->prepare('SELECT * FROM hitbox_point WHERE hitboxId = ? ORDER BY weight') or die(mysql_error());
		$queryHitboxesPoints->execute(array($datasHitboxes['id']));
		for($nbHitboxPoints = 0; $datasHitboxesPoints = $queryHitboxesPoints->fetch(); $nbHitboxPoints++)
		{
			$points[$nbHitboxPoints] = array();
			$points[$nbHitboxPoints]['x'] = intval($datasHitboxesPoints['x']);
			$points[$nbHitboxPoints]['y'] = intval($datasHitboxesPoints['y']);
		}
		
		if(count($points))
		{
			//INVERT Y
			$currentHitboxHeight = 0;
			for($j = 0; $j < count($points); $j++)
			{
				if($currentHitboxHeight < $points[$j]['y'])
					$currentHitboxHeight = $points[$j]['y'];
			}
			for($j = 0; $j < count($points); $j++)
			{
				$points[$j]['y'] = $currentHitboxHeight - $points[$j]['y'];
			}
			
			//POINTS IN INVERSE CLOCKWISE ORDER
			$delta = ($points[0]['x']*$points[1]['y'] + $points[2]['x']*$points[0]['y'] + $points[1]['x']*$points[2]['y']) - ($points[1]['x']*$points[0]['y'] + $points[2]['x']*$points[1]['y'] + $points[0]['x']*$points[2]['y']);
			if($delta < 0)
				$points = array_reverse($points);
				
			$JsonObject['bodies'][$newBodyIdx]['type'] = 2;
			$JsonObject['bodies'][$newBodyIdx]['points'] = $points;
		}
		
	}
}

//GAME OBJECTS
$queryElements = $bdd->prepare('SELECT * FROM element ORDER BY id DESC') or die(mysql_error());
$queryElements->execute();
for($gameObjectNum = 0; $datasElements = $queryElements->fetch(); $gameObjectNum++)
{
	$idToIndex['game-objects'][$datasElements['id']] = $gameObjectNum;
	
	$newGameObjectIdx = count($JsonObject['game-objects']);
	$JsonObject['game-objects'][$newGameObjectIdx] = array();
	
	for($type = 2; $type < 11; $type++)
	{			
		if($type == 2)
		{//Bodies
		
			$queryHitboxes = $bdd->prepare('SELECT * FROM element_hitbox WHERE elementId = ? ORDER BY id DESC') or die(mysql_error());
			$queryHitboxes->execute(array($datasElements['id']));
			for($nb_hitbox = 0; $datasHitboxes = $queryHitboxes->fetch(); $nb_hitbox++)
			{
				if(!isset($JsonObject['game-objects'][$newGameObjectIdx][$type]))
					$JsonObject['game-objects'][$newGameObjectIdx][$type] = array();
					
				$JsonObject['game-objects'][$newGameObjectIdx][$type][$nb_hitbox] = array();
					
				foreach($datasToExport['game-objects-hitbox'] as $key => $val)
				{
					if($key == 'y')
						$datasAssets[$key] = -$datasAssets[$key];
						
					if($key == 'hitbox')
						$dbValue = parseToNumeric($idToIndex['bodies'][$datasHitboxes['hitboxId']]);
					else
						$dbValue = parseToNumeric($datasAssets[$key], $key);
				
					if(!is_null($dbValue))
						$JsonObject['game-objects'][$newGameObjectIdx][$type][$nb_hitbox][$val] = $dbValue;
				}
			}
		}
		else if($type == 3)
		{//skins
			$queryAssets = $bdd->prepare('SELECT asset.width assetWidth, asset.height assetHeight, element_asset.* FROM element_asset INNER JOIN asset ON element_asset.assetId = asset.id WHERE elementId = ? ORDER BY id DESC') or die(mysql_error());
			$queryAssets->execute(array($datasElements['id']));
			for($nb_asset = 0; $datasAssets = $queryAssets->fetch(); $nb_asset++)
			{				
				if(!isset($JsonObject['game-objects'][$newGameObjectIdx][$type]))
					$JsonObject['game-objects'][$newGameObjectIdx][$type] = array();
					
				$JsonObject['game-objects'][$newGameObjectIdx][$type][$nb_asset] = array();
					
				foreach($datasToExport['game-objects-asset'] as $key => $val)
				{
				
					if($key == 'y')
						$datasAssets[$key] = -$datasAssets[$key];
						
					if($key == 'asset')
						$dbValue = parseToNumeric($idToIndex['assets'][$datasAssets['assetId']]);
					else
						$dbValue = parseToNumeric($datasAssets[$key], $key);
				
					if(!is_null($dbValue))
						$JsonObject['game-objects'][$newGameObjectIdx][$type][$nb_asset][$val] = $dbValue;
				}
			}
		}
		else if($type == 4)
		{//collectible
			$value = parseToNumeric($datasElements['collectible']-1, 'collectible');
			if(!is_null($value))
				$JsonObject['game-objects'][$newGameObjectIdx][$type] = $value;
		}
		else if($type == 5)
		{//Breakable
			$value = parseToNumeric($datasElements['breakable'], 'breakable');
			if(!is_null($value))
				$JsonObject['game-objects'][$newGameObjectIdx][$type] = $value;
		}
		else if($type == 6)
		{//Damage
			$value = parseToNumeric($datasElements['damage'], 'damage');
			if(!is_null($value))
				$JsonObject['game-objects'][$newGameObjectIdx][$type] = $value;
		}
		else if($type == 7)
		{//levelsuccess
			$value = parseToNumeric($datasElements['levelsucces']-1, 'levelsucces');
			if(!is_null($value))
				$JsonObject['game-objects'][$newGameObjectIdx][$type] = $value;
		}
		else if($type == 8)
		{//sensor & resitution			
			foreach($datasToExport['collisionsProperties'] as $key => $val)
			{
				$dbValue = parseToNumeric($datasElements[$key], $key);
				if(!is_null($dbValue))
					$JsonObject['game-objects'][$newGameObjectIdx][$type][$val] = $dbValue;
			}
		}
		else if($type == 9)
		{//launcher
			$value = parseToNumeric($datasElements['launcher'], 'launcher');
			if(!is_null($value))
				$JsonObject['game-objects'][$newGameObjectIdx][$type] = $value;
		}
		
	}
}

//GROUPS
$queryGroups = $bdd->query('SELECT * FROM groups ORDER BY id DESC') or die(mysql_error());
for($groupNum = 0; $datasGroups = $queryGroups->fetch(); $groupNum++)
{
	$idToIndex['groups'][$datasGroups['id']] = $groupNum;	
	
	$newGroupsIdx = count($JsonObject['groups']);
	$JsonObject['groups'][$newGroupsIdx] = array();
	
	$queryGroupElements = $bdd->prepare('SELECT * FROM group_element WHERE groupId = ? ORDER BY id DESC') or die(mysql_error());
	$queryGroupElements->execute(array($datasGroups['id']));
	while($datasGroupElements = $queryGroupElements->fetch())
	{
	
		$newGroupElementsIdx = count($JsonObject['groups'][$newGroupsIdx]);
		$JsonObject['groups'][$newGroupsIdx][$newGroupElementsIdx] = array();
		
		$elementSize = getElementSize($idToIndex['game-objects'][$datasGroupElements['elementId']]);
			
		if($datasGroupElements['actionactive'])
		{
			$JsonObject['groups'][$newGroupsIdx][$newGroupElementsIdx]['animation'] = array();
			foreach($datasToExport['groupElementsAnimation'] as $key => $val)
			{
				if($key == 'actionpositiony')
					$datasLayoutElements[$key] = -$datasLayoutElements[$key];
					
				$dbValue = parseToNumeric($datasGroupElements[$key], $key);
				if(!is_null($dbValue))
					$JsonObject['groups'][$newGroupsIdx][$newGroupElementsIdx]['animation'][$val] = $dbValue;
			}
		}
			
		foreach($datasToExport['groupElements'] as $key => $val)
		{
			if($key == 'elementId')
			{
				$dbValue = parseToNumeric($idToIndex['game-objects'][$datasGroupElements[$key]]);
			}
			else if($key == 'anchorX')
			{
				$dbValue = $datasGroupElements[$key] * $elementSize['width'];
			}
			else if($key == 'anchorY')
			{
				$dbValue = (1 - $datasGroupElements[$key]) * $elementSize['height'];
			}
			else
			{
				$dbValue = parseToNumeric($datasGroupElements[$key], $key);
			}
			
			if(!is_null($dbValue))
				$JsonObject['groups'][$newGroupsIdx][$newGroupElementsIdx][$val] = $dbValue;
		}
	}
	
	//INVERT Y
	$groupSize = getGroupSize($newGroupsIdx, $save);
	for($i = 0; $i < count($JsonObject['groups'][$newGroupsIdx]); $i++)
	{
		$gameObjectIndex = $JsonObject['groups'][$newGroupsIdx][$i]['game-object'];
		$elementSize = getElementSize($gameObjectIndex);
		
		$gameObjectY = isset($JsonObject['groups'][$newGroupsIdx][$i]['y']) ? $JsonObject['groups'][$newGroupsIdx][$i]['y'] : 0;
		$gameObjectX = isset($JsonObject['groups'][$newGroupsIdx][$i]['x']) ? $JsonObject['groups'][$newGroupsIdx][$i]['x'] : 0;
		
		$gameObjectAnchorY = isset($JsonObject['groups'][$newGroupsIdx][$i]['anchorY']) ? $JsonObject['groups'][$newGroupsIdx][$i]['anchorY'] : 0;				
		$save['groups'][$newGroupsIdx][$i]['y'] = $gameObjectY;
		$save['groups'][$newGroupsIdx][$i]['x'] = $gameObjectX;
		
		$JsonObject['groups'][$newGroupsIdx][$i]['y'] = ($groupSize['height'] - $gameObjectY);
		$JsonObject['groups'][$newGroupsIdx][$i]['x'] = $gameObjectX;
	}

}

//LEVELS
if(isset($_GET['all']))
	$where = ' WHERE 1 ';
else
	$where = ' WHERE export = 1 ';
	
$queryLevels = $bdd->query('SELECT * FROM level '.$where.' ORDER BY id') or die(mysql_error());
if($queryLevels->rowCount() == 0)
{
	$queryLevels = $bdd->query('SELECT * FROM level ORDER BY id') or die(mysql_error());
}

while($datasLevels = $queryLevels->fetch())
{
	$newLevelsIdx = count($JsonObject['levels']);
	$JsonObject['levels'][$newLevelsIdx] = array();
	
	//LEVEL'S LAYERS
	$queryLevelLayouts = $bdd->prepare('SELECT * FROM level_layout WHERE levelId = ? ORDER BY weight') or die(mysql_error());
	$queryLevelLayouts->execute(array($datasLevels['id']));
	while($datasLevelLayouts = $queryLevelLayouts->fetch())
	{
		$newLayersIdx = count($JsonObject['levels'][$newLevelsIdx]);
		$JsonObject['levels'][$newLevelsIdx][$newLayersIdx] = array();
	
		//LAYER'S ELEMENTS
		$queryLayoutElements = $bdd->prepare('SELECT element.name, element.color, layout_element.* FROM layout_element INNER JOIN element ON elementId = element.id WHERE layoutId = ? ORDER BY id DESC') or die(mysql_error());
		$queryLayoutElements->execute(array($datasLevelLayouts['id']));
		while($datasLayoutElements = $queryLayoutElements->fetch())
		{			
			$newLayersElementsIdx = count($JsonObject['levels'][$newLevelsIdx][$newLayersIdx]);
			$JsonObject['levels'][$newLevelsIdx][$newLayersIdx][$newLayersElementsIdx] = array();
			
			$JsonObject['levels'][$newLevelsIdx][$newLayersIdx][$newLayersElementsIdx]['game-object'] = $idToIndex['game-objects'][$datasLayoutElements['elementId']];
			
			$elementSize = getElementSize($idToIndex['game-objects'][$datasLayoutElements['elementId']]);
			
			exportAnimation($datasLayoutElements, $JsonObject['levels'][$newLevelsIdx][$newLayersIdx][$newLayersElementsIdx]);
			
			foreach($datasToExport['layoutElements'] as $key => $val)
			{
				if($key == 'anchorX')
				{
					$datasLayoutElements[$key] = $datasLayoutElements[$key] * $elementSize['width'];
				}
				if($key == 'anchorY')
				{
					$datasLayoutElements[$key] = (1 - $datasLayoutElements[$key]) * $elementSize['height'];
				}
				
				if($key == 'y')
					$datasLayoutElements[$key] = 1024 - $datasLayoutElements[$key];
					
				$dbValue = parseToNumeric($datasLayoutElements[$key], $key);
				
				if(!is_null($dbValue))
					$JsonObject['levels'][$newLevelsIdx][$newLayersIdx][$newLayersElementsIdx][$val] = $dbValue;
			}
		}	
		
		//LAYER'S GROUPS
		$queryLayoutGroups = $bdd->prepare('SELECT groups.name, layout_group.* FROM layout_group INNER JOIN groups ON groupId = groups.id WHERE layoutId = ? ORDER BY id DESC') or die(mysql_error());
		$queryLayoutGroups->execute(array($datasLevelLayouts['id']));
		while($datasLayoutGroups = $queryLayoutGroups->fetch())
		{	
			$newLayersElementsIdx = count($JsonObject['levels'][$newLevelsIdx][$newLayersIdx]);
			$JsonObject['levels'][$newLevelsIdx][$newLayersIdx][$newLayersElementsIdx] = array();
			
			$JsonObject['levels'][$newLevelsIdx][$newLayersIdx][$newLayersElementsIdx]['group'] = $idToIndex['groups'][$datasLayoutGroups['groupId']];
			
		
			$groupSize = getGroupSize($idToIndex['groups'][$datasLayoutGroups['groupId']], $save);

			exportAnimation($datasLayoutGroups, $JsonObject['levels'][$newLevelsIdx][$newLayersIdx][$newLayersElementsIdx]);
			
			foreach($datasToExport['layoutGroups'] as $key => $val)
			{
				if($key == 'x')
					$datasLayoutGroups[$key] = $datasLayoutGroups[$key];
				if($key == 'y')
					$datasLayoutGroups[$key] = 1024 - $datasLayoutGroups[$key];
				if($key == 'anchorX')
					$datasLayoutGroups[$key] = $datasLayoutGroups[$key] * ($groupSize['width']-$groupSize['x']) + $groupSize['x'];
				if($key == 'anchorY')
					$datasLayoutGroups[$key] = (1 - $datasLayoutGroups[$key]) * ($groupSize['height']-$groupSize['y']);
					
				$dbValue = parseToNumeric($datasLayoutGroups[$key], $key);
				
				if(!is_null($dbValue))
					$JsonObject['levels'][$newLevelsIdx][$newLayersIdx][$newLayersElementsIdx][$val] = $dbValue;
			}
			
		}	
	}
}

//OUTPUT
echo json_encode($JsonObject);
?>