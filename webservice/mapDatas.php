<?php

if(isset($_POST['datas']))
	$_POST = json_decode($_POST['datas'], true);
	
include './init.php';

$retourJSON = array();
$retourJSON['mapLevels'] = array();
$mapLevelFields = array('parentId', 'type', 'x', 'y');

//SAVE
if(isset($_POST['save']) && isset($_GET['dev']))
{
	$queryLevels = $bdd->query('SELECT * FROM map_level ORDER BY levelId') or die(mysql_error());
	while($datasLevels = $queryLevels->fetch())
	{
		foreach($_POST['mapLevels'] as $key => $currentValue)
		{
			if(isset($currentValue['levelId']) && $currentValue['levelId'] == $datasLevels['levelId'])
			{
				$updateSqlFields = '';
				$updateValues = array();
				
				$fieldsNumber = 0;
				foreach($mapLevelFields as $fieldName)
				{
					if($datasLevels[$fieldName] != $currentValue[$fieldName])
					{
						if($fieldsNumber > 0)
							$updateSqlFields .= ', ';
							
						$fieldsNumber++;
					
						$updateSqlFields .= $fieldName.'=?';
					
						$updateValues[] = $currentValue[$fieldName];
					}
				}
				
				//ICI A FAIRE
				//echo $key.' => '.count($updateValues)
				
				if(count($updateValues))
				{
					$updateSql = 'UPDATE map_level SET '.$updateSqlFields.' WHERE levelId=?';
					$updateValues[] = $datasLevels['levelId'];

					$updateLevel = $bdd->prepare($updateSql) or die(mysql_error());
					$updateLevel->execute($updateValues);
				}
			}
			
			unset($_POST['mapLevels'][$key]);
		}
	}
	
	if(count($_POST['mapLevels']))
	{
		foreach($_POST['mapLevels'] as $currentKey => $currentValue)
		{
			$insertSqlFields = 'levelId';
			$questionMarks = '?';
			$insertValues = array($currentValue['levelId']);
			
			foreach($mapLevelFields as $idx => $fieldName)
			{
				$insertSqlFields .= ', '.$fieldName;
				$questionMarks .= ', ?';
				$insertValues[] = $currentValue[$fieldName];				
			}
		
			$insertSql = 'INSERT INTO map_level('.$insertSqlFields.') VALUES('.$questionMarks.')';
			
			$insertLevel = $bdd->prepare($insertSql) or die(mysql_error());
			$insertLevel->execute($insertValues);			
		}
	}
}

//GET MAP'S LEVELS
$queryLevels = $bdd->query('SELECT level.id, level.name, map_level.* FROM level LEFT OUTER JOIN map_level ON level.id = map_level.levelId ORDER BY parentId') or die(mysql_error());
while($datasLevels = $queryLevels->fetch())
{
	$datasLevels['levelId'] = $datasLevels['id'];
	$retourJSON['mapLevels'][] = $datasLevels;
}

echo json_encode($retourJSON);
?>