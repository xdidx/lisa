<?php

if(isset($_POST['datas']))
	$_POST = json_decode($_POST['datas'], true);
	
include './init.php';

$retourJSON = array();

$contentsList['levels'] = array('name', 'export');
$contentsList['levelLayouts'] = array('name', 'levelId', 'weight');
$contentsList['layoutElements'] = array('layoutId', 'elementId', 'x', 'y', 'scaleX', 'scaleY',  'anchorX', 'anchorY', 'rotation', 'actionactive', 'actionpositionx', 'actionpositiony', 'actionrotation', 'actiontime', 'actionloop', 'actionreverse', 'actionbeforereverse', 'actionbeforeactive');
$contentsList['layoutGroups'] = array('layoutId', 'groupId', 'x', 'y', 'scaleX', 'scaleY',  'anchorX', 'anchorY', 'rotation', 'actionactive', 'actionpositionx', 'actionpositiony', 'actionrotation', 'actiontime', 'actionloop', 'actionreverse', 'actionbeforereverse', 'actionbeforeactive');

$contentsList['elements'] = array('name', 'color', 'levelsucces', 'restitution', 'sensor', 'collectible', 'damage', 'breakable', 'launcher');
$contentsList['elementHitboxes'] = array('x', 'y', 'elementId', 'hitboxId');
$contentsList['elementAssets'] = array('scaleX', 'scaleY', 'x', 'y', 'elementId', 'assetId');

$contentsList['groups'] = array('name');
$contentsList['groupElements'] = array('groupId', 'elementId', 'x', 'y', 'scaleX', 'scaleY', 'anchorX', 'anchorY', 'rotation', 'actionactive', 'actionpositionx', 'actionpositiony', 'actionrotation', 'actiontime', 'actionloop', 'actionreverse', 'actionbeforereverse', 'actionbeforeactive');

$contentsList['hitboxes'] = array('name', 'type', 'width', 'height', 'x', 'y', 'modeleId');
$contentsList['hitboxPoints'] = array('hitboxId', 'x', 'y', 'weight');
$contentsList['assets'] = array('name');
//$contentsList['scripts'] = array('inverse', 'loop');
//$contentsList['scriptSteps'] = array('scriptId', 'time', 'x', 'y', 'width', 'height');

$dbTableName['levels'] = 'level';
$dbTableName['levelLayouts'] = 'level_layout';
$dbTableName['layoutElements'] = 'layout_element';
$dbTableName['layoutGroups'] = 'layout_group';

$dbTableName['elements'] = 'element';
$dbTableName['elementHitboxes'] = 'element_hitbox';
$dbTableName['elementAssets'] = 'element_asset';


$dbTableName['groups'] = 'groups';
$dbTableName['groupElements'] = 'group_element';

$dbTableName['hitboxes'] = 'hitbox';
$dbTableName['hitboxPoints'] = 'hitbox_point';

$dbTableName['assets'] = 'asset';

$pointWidth = 10;

foreach($contentsList as $key => $value)
	$retourJSON[$key] = array();

$retourJSON['openCategories'] = array();
if(isset($_COOKIE['openCatgories']))
	$retourJSON['openCategories'] = json_decode($_COOKIE['openCatgories']);
	
//SAVE
if(isset($_POST['save']) || isset($_GET['save']))
{
	if(isset($_COOKIE['cookieSave']))
		setcookie('cookieSave', 'null', time() - 3600);
		
	//setcookie('openCatgories', json_encode($_POST['openCategories']), time() + (24 * 3600 * 10));
	//$retourJSON['openCategories'] = $_POST['openCategories'];
	$retourJSON['openCategories'] = 0;
	
	$newLevel = false;
	echo count($contentsList);
	foreach($contentsList as $key => $value)
	{
		echo $key;
		$queryLevels = $bdd->query('SELECT * FROM '.$dbTableName[$key].' ORDER BY id') or die(mysql_error());
		
		while($datasLevels = $queryLevels->fetch())
		{
			$deleted = true;
			
			if(count($_POST[$key]) > 0)
			{
				foreach($_POST[$key] as $currentKey => $currentValue)
				{
					if(isset($currentValue['id']) && $currentValue['id'] == $datasLevels['id'])
					{
						$updateSqlFields = '';
						$updateValues = array();
						
						$fieldsNumber = 0;
						foreach($value as $idx => $fieldName)
						{
							if($datasLevels[$fieldName] != $currentValue[$fieldName])
							{
								if($fieldsNumber > 0)
									$updateSqlFields .= ', ';
									
								$fieldsNumber++;
							
								$updateSqlFields .= $fieldName.'=?';
							
								$updateValues[] = $currentValue[$fieldName];
							}
						}
						
						if(count($updateValues))
						{
							$updateSql = 'UPDATE '.$dbTableName[$key].' SET '.$updateSqlFields.' WHERE id=?';
							$updateValues[] = $datasLevels['id'];

							$updateLevel = $bdd->prepare($updateSql) or die(mysql_error());
							$updateLevel->execute($updateValues);
						}
						
						$deleted = false;
						unset($_POST[$key][$currentKey]);
					}
				}
			}
			
			if($deleted)
			{
				$deleteSql = 'DELETE FROM '.$dbTableName[$key].' WHERE id=?';				
				$deleteValues = array($datasLevels['id']);
				
				$deleteLevel = $bdd->prepare($deleteSql) or die(mysql_error());
				$deleteLevel->execute($deleteValues);
			}
			
		}
	
		if(isset($_POST[$key]) && count($_POST[$key]))
		{
			foreach($_POST[$key] as $currentKey => $currentValue)
			{
				$insertSqlFields = '';
				$questionMarks = '';
				$insertValues = array();
				foreach($value as $idx => $fieldName)
				{
					if($idx > 0)
					{
						$insertSqlFields .= ', ';
						$questionMarks .= ', ';
					}
					$insertSqlFields .= $fieldName;
					
					$insertValues[] = $currentValue[$fieldName];
					$questionMarks .= '?';
				}
			
				$insertSql = 'INSERT INTO '.$dbTableName[$key].'('.$insertSqlFields.') VALUES('.$questionMarks.')';
				
				$insertLevel = $bdd->prepare($insertSql) or die(mysql_error());
				$insertLevel->execute($insertValues);
				
				if($key == 'levels' || true)
				{
					$newLevel = $bdd->lastInsertId();
				}			
				
			}
		}
	}
	
	if($newLevel)
	{
		$requete = $bdd->query("INSERT INTO level_layout(name, levelId, weight) VALUES('Front', $newLevel, 0)");
		
		$newLayoutId = $bdd->lastInsertId();		
		$insertElementsSql = "INSERT INTO layout_element (layoutId, elementId, x, y, scaleX, scaleY, anchorX, anchorY, rotation, actionactive, actionpositionx, actionpositiony, actionrotation, actiontime, actionloop, actionreverse, actionbeforeactive, actionbeforereverse) VALUES
		($newLayoutId, 111, 672, 480, '1.00', '1.00', '0.00', '0.00', 0, 0, 0, 0, 0, '1.000', 1, 1, 0, 0),
		($newLayoutId, 111, 384, 128, '1.00', '1.00', '0.00', '0.00', 0, 0, 0, 0, 0, '1.000', 1, 1, 0, 0),
		($newLayoutId, 111, 160, 448, '1.00', '1.00', '0.00', '0.00', 0, 0, 0, 0, 0, '1.000', 1, 1, 0, 0),
		($newLayoutId, 110, 544, 32, '1.00', '1.00', '0.00', '0.00', 0, 0, 0, 0, 0, '1.000', 1, 1, 0, 0),
		($newLayoutId, 121, 311, 849, '1.00', '1.00', '0.00', '0.00', 0, 0, 0, 0, 0, '1.000', 1, 1, 0, 0)";
		$reqElements = $bdd->query($insertElementsSql);
	}
}

//GET DATAS
//LEVELS, LEVEL'S LAYOUTS, LAYOUT'S ELEMENTS LAYOUT'S GRUOPS
$queryLevels = $bdd->query('SELECT * FROM level ORDER BY id DESC') or die(mysql_error());
while($datasLevels = $queryLevels->fetch())
{
	$retourJSON['levels'][] = $datasLevels;
	$queryLevelLayouts = $bdd->prepare('SELECT * FROM level_layout WHERE levelId = ? ORDER BY weight DESC') or die(mysql_error());
	$queryLevelLayouts->execute(array($datasLevels['id']));
	while($datasLevelLayouts = $queryLevelLayouts->fetch())
	{
		$retourJSON['levelLayouts'][] = $datasLevelLayouts;
	
		$queryLayoutElements = $bdd->prepare('SELECT element.name, element.color, layout_element.* FROM layout_element INNER JOIN element ON elementId = element.id WHERE layoutId = ? ORDER BY id DESC') or die(mysql_error());
		$queryLayoutElements->execute(array($datasLevelLayouts['id']));
		while($datasLayoutElements = $queryLayoutElements->fetch())
		{
			$retourJSON['layoutElements'][] = $datasLayoutElements;
		}
	
		$queryLayoutGroups = $bdd->prepare('SELECT groups.name, layout_group.* FROM layout_group INNER JOIN groups ON groupId = groups.id WHERE layoutId = ? ORDER BY id DESC') or die(mysql_error());
		$queryLayoutGroups->execute(array($datasLevelLayouts['id']));
		while($datasLayoutGroups = $queryLayoutGroups->fetch())
		{
			$retourJSON['layoutGroups'][] = $datasLayoutGroups;
		}
	}
}

//ELEMENTS, ELEMENT'S HITBOXES, ELEMENT'S ASSETS
$queryElements = $bdd->prepare('SELECT * FROM element ORDER BY id DESC') or die(mysql_error());
$queryElements->execute();
while($datasElements = $queryElements->fetch())
{
	$retourJSON['elements'][] = $datasElements;
	
	$queryElementHitboxes = $bdd->prepare('SELECT * FROM element_hitbox WHERE elementId = ? ORDER BY id DESC') or die(mysql_error());
	$queryElementHitboxes->execute(array($datasElements['id']));
	while($datasElementHitboxes = $queryElementHitboxes->fetch())
	{
		$queryHitboxes = $bdd->prepare('SELECT * FROM hitbox WHERE id = ?') or die(mysql_error());
		$queryHitboxes->execute(array($datasElementHitboxes['hitboxId']));
		if($datasHitboxes = $queryHitboxes->fetch())
		{
			$datasElementHitboxes['name'] = $datasHitboxes['name'];
			$retourJSON['elementHitboxes'][] = $datasElementHitboxes;
		}
	}
	
	$queryElementAssets = $bdd->prepare('SELECT * FROM element_asset WHERE elementId = ? ORDER BY id DESC') or die(mysql_error());
	$queryElementAssets->execute(array($datasElements['id']));
	while($datasElementAssets = $queryElementAssets->fetch())
	{	
		$queryAssets = $bdd->prepare('SELECT * FROM asset WHERE id = ?') or die(mysql_error());
		$queryAssets->execute(array($datasElementAssets['assetId']));
		if($datasAssets = $queryAssets->fetch())
		{
			$datasElementAssets['name'] = $datasAssets['name'];
			$retourJSON['elementAssets'][] = $datasElementAssets;
		}
	}	
}

//ASSETS
$queryAssets = $bdd->prepare('SELECT * FROM asset ORDER BY id DESC') or die(mysql_error());
$queryAssets->execute();
while($datasAssets = $queryAssets->fetch())
{
	$retourJSON['assets'][] = $datasAssets;
}

//HITBOXES
$queryHitboxes = $bdd->prepare('SELECT * FROM hitbox ORDER BY id DESC') or die(mysql_error());
$queryHitboxes->execute();
while($datasHitboxes = $queryHitboxes->fetch())
{
	$retourJSON['hitboxes'][] = $datasHitboxes;
	
	$queryHitboxPoints = $bdd->prepare('SELECT * FROM hitbox_point WHERE hitboxId = ? ORDER BY weight') or die(mysql_error());
	$queryHitboxPoints->execute(array($datasHitboxes['id']));
	for($i = 0; $datasHitboxPoints = $queryHitboxPoints->fetch() OR $i < 3; $i++)
	{
		if(!isset($datasHitboxPoints['hitboxId']))
		{
			$datasHitboxPoints['hitboxId'] = $datasHitboxes['id'];
			$datasHitboxPoints['x'] = ($i+1) * 10;
			$datasHitboxPoints['y'] = ($i+1) * 10;
		}
		
		$datasHitboxPoints['name'] = "Point $i";
		$datasHitboxPoints['weight'] = "$i";
		$datasHitboxPoints['midPoint'] = 'true';
		$retourJSON['hitboxPoints'][] = $datasHitboxPoints;
	}
}

//GROUPS
$queryGroups = $bdd->prepare('SELECT * FROM groups ORDER BY id DESC') or die(mysql_error());
$queryGroups->execute();
while($datasGroups = $queryGroups->fetch())
{
	$retourJSON['groups'][] = $datasGroups;
	
	$queryGroupElements = $bdd->prepare('SELECT element.name, group_element.* FROM group_element INNER JOIN element ON element.id = group_element.elementId WHERE groupId = ? ORDER BY id DESC') or die(mysql_error());
	$queryGroupElements->execute(array($datasGroups['id']));
	while($datasGroupElements = $queryGroupElements->fetch())
	{
		$retourJSON['groupElements'][] = $datasGroupElements;
	}	
}
echo json_encode($retourJSON);
?>