<html>

	<head>
		<title>Lisa</title>
		<link rel="stylesheet" media="all" type="text/css" rel="stylesheet" href="includes/main.css" />
		<link rel="stylesheet" media="all" type="text/css" rel="stylesheet" href="includes/jquery-ui.min.css" />
	</head>
	
	<body>

		<header>
			<input type="button" class="fleft" id="B_save" value="Sauvegarder"/>
			<div id="D_status">
				<div id="D_colorBar"></div>
				<div id="D_statusText"></div>
			</div>
			<div class="fleft">
				<img height="30" src="images/left-arrow.png" alt="navigate Arrow" class="navigate-arrow"/>
				<img height="30" src="images/right-arrow.png" alt="navigate Arrow" class="navigate-arrow"/>
			</div>
		</header>
		
		<!--LEVELS-->
		<div id="D_levelCreator" class="categorie" >
		
			<div class="titre">
				<span class="real-title">Gestion des niveaux</span> <span id="zoomValue"></span>
			</div>
			
			<!--<canvas class="container" id="C_levelImage" height="800" width="600" ></canvas>-->
			<div class="container" id="C_levelImage" ></div>
			
			<!--LAYOUT ELEMENTS-->
			<div class="fleft">
				<div id="D_layoutElementsOptions" class="module" style="background-color:#2ECCFA;">
					<select id="S_newElementsList">
						<option>Chargement...</option>
					</select>
					<br/>					
					<input class="I_add I_layoutElements needSelectedLevelLayouts" type="button" value="Ajouter l'�l�ment" />
					<br/>
					<input class="I_remove I_layoutElements needSelectedLayoutElements" type="button" value="Supprimer l'�l�ment" />
					<br/>
					Layout :
					<select id="TB_layoutElements_layoutId" class="TB_editProperty needSelectedLayoutElements">
						<option>Chargement...</option>
					</select>
					
					<br/>
					Position :
					<br/>
					X : <input id="TB_layoutElements_x" class="TB_editProperty needSelectedLayoutElements" type="number" value="0" /> 
					<br/>
					Y : <input id="TB_layoutElements_y" class="TB_editProperty needSelectedLayoutElements" type="number" value="0" />
					<br/>
					Taille : 
					<br/>
					Garder les proportions : <input type="checkbox" id="CB_proportionsLayoutElements" checked="checked" />
					<br/>			
					Rotation : <input id="TB_layoutElements_rotation" class="TB_editProperty needSelectedLayoutElements" type="number" value="0" />
					<br/>
					Anchor X : <input id="TB_layoutElements_anchorX" class="TB_editProperty needSelectedLayoutElements" type="number" step="0.05" value="0" />
					<br/>
					Anchor Y : <input id="TB_layoutElements_anchorY" class="TB_editProperty needSelectedLayoutElements" type="number" step="0.05" value="0" />
					
					<hr/>
					
					Activer l'animation : <input type="checkbox" id="TB_layoutElements_actionactive" class="TB_editProperty needSelectedLayoutElements">
					<br/>
					Temps avant anim. : <input type="number" id="TB_layoutElements_actionbeforeactive" class="TB_editProperty needSelectedLayoutElements">
					<br/>
					Position X : <input type="number" id="TB_layoutElements_actionpositionx" class="TB_editProperty needSelectedLayoutElements">
					<br/>
					Position Y : <input type="number" id="TB_layoutElements_actionpositiony" class="TB_editProperty needSelectedLayoutElements">
					<br/>
					Rotation : <input type="number" id="TB_layoutElements_actionrotation" class="TB_editProperty needSelectedLayoutElements">
					<br/>
					Temps : <input type="number" id="TB_layoutElements_actiontime" step="0.01" class="TB_editProperty needSelectedLayoutElements">
					<br/>
					Boucle : <input type="checkbox" id="TB_layoutElements_actionloop" class="TB_editProperty needSelectedLayoutElements">
					<br/>
					Inverser : <input type="checkbox" id="TB_layoutElements_actionreverse" class="TB_editProperty needSelectedLayoutElements">
					<br/>
					Temps avant inv. : <input type="number" id="TB_layoutElements_actionbeforereverse" step="0.1" class="TB_editProperty needSelectedLayoutElements">
				</div>
				
				<div id="D_layoutElementsList" style="background-color:#2ECCFA;" class="list" >
					Chargement en cours...
				</div>			
			</div>
			
			<!--LAYOUT GROUPS-->
			<div class="fleft">
				<div id="D_layoutGroupsOptions" style="background-color:#2ECCFA;" class="module">
					<select id="S_newGroupsList">
						<option>Chargement...</option>
					</select>					
					<br/>
					<input class="I_add I_layoutGroups needSelectedLevelLayouts" type="button" value="Ajouter l'�l�ment" />
					<br/>
					<input class="I_remove I_layoutGroups needSelectedLayoutGroups" type="button" value="Supprimer l'�l�ment" />
					<br/>
					Layout :
					<select id="TB_layoutGroups_layoutId" class="TB_editProperty needSelectedLayoutGroups">
						<option>Chargement...</option>
					</select>
					
					<br/>
					Position :
					<br/>
					X : <input id="TB_layoutGroups_x" class="TB_editProperty needSelectedLayoutGroups" type="number" value="0" /> 
					<br/>
					Y : <input id="TB_layoutGroups_y" class="TB_editProperty needSelectedLayoutGroups" type="number" value="0" />
					<br/>
					Taille : 
					<br/>
					Garder les proportions : <input type="checkbox" id="CB_proportionsLayoutGroups" checked="checked" />
					<br/>			
					Rotation : <input id="TB_layoutGroups_rotation" class="TB_editProperty needSelectedLayoutGroups" type="number" value="0" />
					<br/>
					Anchor X : <input id="TB_layoutGroups_anchorX" class="TB_editProperty needSelectedLayoutGroups" type="number" step="0.05" value="0" />
					<br/>
					Anchor Y : <input id="TB_layoutGroups_anchorY" class="TB_editProperty needSelectedLayoutGroups" type="number" step="0.05" value="0" />
					
					<hr/>
					
					Activer l'animation : <input type="checkbox" id="TB_layoutGroups_actionactive" class="TB_editProperty needSelectedLayoutGroups">
					<br/>
					Temps avant anim. : <input type="number" id="TB_layoutGroups_actionbeforeactive" class="TB_editProperty needSelectedLayoutGroups">
					<br/>
					Position X : <input type="number" id="TB_layoutGroups_actionpositionx" class="TB_editProperty needSelectedLayoutGroups">
					<br/>
					Position Y : <input type="number" id="TB_layoutGroups_actionpositiony" class="TB_editProperty needSelectedLayoutGroups">
					<br/>
					Rotation : <input type="number" id="TB_layoutGroups_actionrotation" class="TB_editProperty needSelectedLayoutGroups">
					<br/>
					Temps : <input type="number" id="TB_layoutGroups_actiontime" step="0.01" class="TB_editProperty needSelectedLayoutGroups">
					<br/>
					Boucle : <input type="checkbox" id="TB_layoutGroups_actionloop" class="TB_editProperty needSelectedLayoutGroups">
					<br/>
					Inverser : <input type="checkbox" id="TB_layoutGroups_actionreverse" class="TB_editProperty needSelectedLayoutGroups">
					<br/>
					Temps avant inv. : <input type="number" id="TB_layoutGroups_actionbeforereverse" step="0.1" class="TB_editProperty needSelectedLayoutGroups">
				</div>
				
				<div id="D_layoutGroupsList" style="background-color:#2ECCFA;" class="list" >
					Chargement en cours...
				</div>			
			</div>
			
			<div class="fleft">
				<!--NIVEAUX-->
				<div id="D_levelOptions"  style="background-color:#FA5858;" class="module">
					<input class="I_add I_levels" type="button" value="Ajouter un niveau" />
					<input class="I_remove I_levels needSelectedLevels" type="button" value="Supprimer le niveau" />
					<br/>
					Nom : <input id="TB_levels_name" class="TB_editProperty needSelectedLevels" type="text" />
					<br/>
					Exporter : <input id="TB_levels_export" class="TB_editProperty needSelectedLevels" type="checkbox" />
					<br/>
					Cacher points d'ancrage : <input type="checkbox" id="CB_pointsAncrage" />
					<br/>
					Fond noir : <input type="checkbox" id="CB_couleurFond" />
					<br/>
					Activer grille : <input type="checkbox" id="CB_grille" />
					<br/>
					Largeur grille : <input min="1" value="32" type="number" id="TB_largeurGrille" />
					<br/>
					Hauteur grille : <input min="1" value="32" type="number" id="TB_hauteurGrille" />
				</div>
				<div id="D_levelsList" style="background-color:#FA5858;" class="list">
					Chargement en cours...
				</div>				
			
				<!--LAYOUTS-->
				<div id="D_levelLayoutsOptions" style="background-color:#FE9A2E;" class="module">
					<input class="I_add I_levelLayouts needSelectedLevels" type="button" value="Ajouter un layout" />
					<input class="I_remove I_levelLayouts needSelectedLevelLayouts" type="button" value="Supprimer le layout" />
					<br/>
					Nom : <input id="TB_levelLayouts_name" class="TB_editProperty needSelectedLevelLayouts" type="text" />
					<br/>
					<input id="TB_levelLayouts_orderPlus" class="needSelectedLevelLayouts" type="button" value="Faire monter" />
					<br/>
					<input id="TB_levelLayouts_orderMinus" class="needSelectedLevelLayouts" type="button" value="Faire baisser" />
				</div>
				<div id="D_levelLayoutsList" style="background-color:#FE9A2E;" class="list">
					Chargement en cours...
				</div>
			</div>
		</div>
		
		<!--ELEMENTS-->
		<div id="D_elementCreator" class="categorie" style="background-color:#5858FA;">
			<div class="titre">
				Gestion des �l�ments
			</div>
			<!--ELEMENTS-->
			<div class="fleft">
				<div id="D_elementsOptions" class="module">
					<input class="I_add I_elements" type="button" value="Ajouter un element" />
					<input class="I_remove I_elements needSelectedElements" type="button" value="Supprimer l'element" />
					<br/>
					Nom : <input id="TB_elements_name" class="TB_editProperty needSelectedElements" type="text" />
					<br/>
					Couleur : 
					<select id="TB_elements_color" class="TB_editProperty needSelectedElements">
						<option value="red">Rouge</option>
						<option value="blue">Bleu</option>
						<option value="green">Vert</option>
						<option value="orange">Orange</option>
					</select>
					<br/>
					Level succes : 
					<select id="TB_elements_levelsucces" class="TB_editProperty needSelectedElements">
						<option value="0">Aucun</option>
						<option value="1">Pigeon</option>
					</select>
					<br/>
					Restitution : <input id="TB_elements_restitution" class="TB_editProperty needSelectedElements" type="number" step="0.1" />
					<br/>
					Sensor : <input id="TB_elements_sensor" class="TB_editProperty needSelectedElements" type="checkbox" />
					<br/>
					Collectible : 
					<select id="TB_elements_collectible" class="TB_editProperty needSelectedElements">
						<option value="0">Aucun</option>
						<option value="1">Etoile</option>
					</select>
					 
					<br/>
					Damage : <input id="TB_elements_damage" class="TB_editProperty needSelectedElements" type="number" />
					<br/>
					Breakable : <input id="TB_elements_breakable" class="TB_editProperty needSelectedElements" type="number" />
					<br/>
					Launcher : <input id="TB_elements_launcher" class="TB_editProperty needSelectedElements" type="checkbox" />
				</div>
				
				<div id="D_elementsList" class="list" >
					Chargement en cours...
				</div>
			</div>
			
			<!--ELEMENT HITBOXES-->
			<div class="fleft">
				<div id="D_elementHitboxesOptions" class="module" >
					<select id="S_newElementHitboxesList">
						<option>Chargement...</option>
					</select>
					<input class="I_add I_elementHitboxes" type="button" value="Ajouter une hitbox" />
					<input class="I_remove I_elementHitboxes needSelectedElementHitboxes" type="button" value="Supprimer la hitbox" />
					<br/>
					x : <input id="TB_elementHitboxes_x" size="1" class="TB_editProperty needSelectedElementHitboxes" type="number" value="0" />
					<br/>				
					y : <input id="TB_elementHitboxes_y" size="1" class="TB_editProperty needSelectedElementHitboxes" type="number" value="0" />
					<br/>
				</div>
				
				<div id="D_elementHitboxesList" class="list">
					Chargement en cours...
				</div>
			</div>
			
			<!--ELEMENT ASSETS-->
			<div class="fleft">
				<div id="D_elementAssetsOptions" class="module">
					<select id="S_newElementAssetsList">
						<option>Chargement...</option>
					</select>
					<input class="I_add I_elementAssets" type="button" value="Ajouter un element" />
					<input class="I_remove I_elementAssets needSelectedElementAssets" type="button" value="Supprimer l'element" />			
					<br/>
					x : <input id="TB_elementAssets_x" size="1" class="TB_editProperty needSelectedElementAssets" type="number" value="0" />
					<br/>				
					y : <input id="TB_elementAssets_y" size="1" class="TB_editProperty needSelectedElementAssets" type="number" value="0" />
				</div>
				
				<div id="D_elementAssetsList" class="list" >
					Chargement en cours...
				</div>
			</div>
			
			<!--<canvas class="container" id="C_elementImage" height="200" width="200" ></canvas>-->
			<div class="container" id="C_elementImage"></div>
			
		</div>
		
		<!--GROUPS-->
		<div id="D_groupCreator" class="categorie" style="background-color:#F7819F;">
			<div class="titre">
				Gestion des groupes
			</div>
			
			<div class="fleft">
				<div id="D_groupsTools" class="module">
					<input class="I_add I_groups" type="button" value="Ajouter un groupe" />
					<input class="I_remove I_groups needSelectedGroups" type="button" value="Supprimer le groupe" />
					<br/>
					Nom : <input id="TB_groups_name" class="TB_editProperty needSelectedGroups" type="text" />
					<br/>
					Activer grille : <input type="checkbox" id="CB_grilleGroupe" />
					<br/>
					Largeur grille : <input min="1" value="32" type="number" id="TB_largeurGrilleGroupe" />
					<br/>
					Hauteur grille : <input min="1" value="32" type="number" id="TB_hauteurGrilleGroupe" />
				</div>	
				<div id="D_groupsList" class="list">
					Chargement en cours...
				</div>
			</div>
			
			<div class="fleft">
				<div id="D_groupElementsTools" class="module">
					<select id="S_newGroupElementsList">
						<option>Chargement...</option>
					</select>
					
					<input class="I_add I_groupElements needSelectedGroups" type="button" value="Ajouter un �l�ment" />
					<input class="I_remove I_groupElements needSelectedGroupElements" type="button" value="Supprimer l'�l�ment" />
					<br/>
					X : <input id="TB_groupElements_x" class="TB_editProperty needSelectedGroupElements" type="number" />
					<br/>
					Y : <input id="TB_groupElements_y" class="TB_editProperty needSelectedGroupElements" type="number" />
					<br/>
					Anchor X : <input id="TB_groupElements_anchorX" class="TB_editProperty needSelectedGroupElements" type="number" step="0.05" />
					<br/>
					Anchor Y : <input id="TB_groupElements_anchorY" class="TB_editProperty needSelectedGroupElements" type="number" step="0.05" />
					<br/>
					Rotation : <input id="TB_groupElements_rotation" class="TB_editProperty needSelectedGroupElements" type="number" />
					
					<hr/>
					
					Activer l'animation : <input type="checkbox" id="TB_groupElements_actionactive" class="TB_editProperty needSelectedGroupElements">
					<br/>
					Temps avant anim. : <input type="number" id="TB_groupElements_actionbeforeactive" class="TB_editProperty needSelectedGroupElements">
					<br/>
					Position X : <input type="number" id="TB_groupElements_actionpositionx" class="TB_editProperty needSelectedGroupElements">
					<br/>
					Position Y : <input type="number" id="TB_groupElements_actionpositiony" class="TB_editProperty needSelectedGroupElements">
					<br/>
					Rotation : <input type="number" id="TB_groupElements_actionrotation" class="TB_editProperty needSelectedGroupElements">
					<br/>
					Temps : <input type="number" id="TB_groupElements_actiontime" step="0.01" class="TB_editProperty needSelectedGroupElements">
					<br/>
					Boucle : <input type="checkbox" id="TB_groupElements_actionloop" class="TB_editProperty needSelectedGroupElements">
					<br/>
					Inverser : <input type="checkbox" id="TB_groupElements_actionreverse" class="TB_editProperty needSelectedGroupElements">
					<br/>
					Temps avant inv. : <input type="number" id="TB_groupElements_actionbeforereverse" step="0.1" class="TB_editProperty needSelectedGroupElements">
				</div>	
				<div id="D_groupElementsList" class="list">
					Chargement en cours...
				</div>
			</div>
			
			<div class="container" id="C_groupImage" ></div>
		</div>
		
		<!--HITBOXES-->
		<div id="D_hitboxCreator" class="categorie" style="background-color:#31B404;">
			<div class="titre">
				Gestion des hitboxes
			</div>
			
			<div class="fleft">				
				<div id="D_hitboxesTools" class="module">
					<input class="I_add I_hitboxes" type="button" value="Ajouter une hitbox" />
					<input class="I_remove I_hitboxes needSelectedHitboxes" type="button" value="Supprimer la hitbox" />
					<br/>
					Mod�le : 
					<select id="TB_hitboxes_modeleId" class="TB_editProperty needSelectedHitboxes">
						<option value="0">Aucun</option>
					</select>
					<br/>
					Largeur : <span id="hitboxModelWidth"></span>
					<br/>
					Hauteur : <span id="hitboxModelHeight"></span>
					<hr/>
					Type d'hitbox : 
					<select id="TB_hitboxes_type" class="TB_editProperty needSelectedHitboxes">
						<option value="0">Rectangle</option>
						<option value="1">Rond</option>
						<option value="2">Polygone</option>
					</select>
					<br/>
					Nom : <input id="TB_hitboxes_name" class="TB_editProperty needSelectedHitboxes" type="text" />
					<div id="D_hitboxes_properties">
						X : <input id="TB_hitboxes_x" class="TB_editProperty needSelectedHitboxes" type="number" value="0" />
						<br/>
						Y : <input id="TB_hitboxes_y" class="TB_editProperty needSelectedHitboxes" type="number" value="0" />
						<br/>
						Largeur hitbox: <input id="TB_hitboxes_width" class="TB_editProperty needSelectedHitboxes" type="number" value="0" min="0" />
						<div id="D_hitboxes_height">
							Hauteur hitbox: <input id="TB_hitboxes_height" class="TB_editProperty needSelectedHitboxes" type="number" value="0" min="0" />
						</div>
					</div>
				</div>	
				
				<div id="D_hitboxesList" class="list">
					Chargement en cours...
				</div>
			</div>
			
			<!--<canvas class="container" id="C_hitboxImage" height="200" width="200" ></canvas>-->
			<div class="container" id="C_hitboxImage" ></div>
			
			<div class="fleft" id="D_hitboxPointsListModule">
				<div id="D_hitboxPointsList" class="list">
					Chargement en cours...
				</div>
				
				<div id="D_hitboxPointsTools" class="module">
					<input class="I_add I_hitboxPoints needSelectedHitboxes" type="button" value="Ajouter un point" />
					<input class="I_remove I_hitboxPoints needSelectedHitboxPoints" type="button" value="Supprimer le point" />
					<br/>
					Position :
					<br/>
					X : <input id="TB_hitboxPoints_x" size="1" class="TB_editProperty needSelectedHitboxPoints" type="number" value="0" /> 
					Y : <input id="TB_hitboxPoints_y" size="1" class="TB_editProperty needSelectedHitboxPoints" type="number" value="0" />
				</div>
			</div>
		</div>
		
		<!--ASSETS-->
		<div id="D_assetCreator" class="categorie" style="background-color:#D7DF01;">
			<div class="titre">
				Gestion des assets
			</div>
			
			<div class="fleft">				
				<div id="D_assetsTools" class="module">			
					<input class="I_add I_assets" type="button" value="Ajouter un asset" />
					<input class="I_remove I_assets needSelectedAssets" type="button" value="Supprimer l'asset" />
					<br/>
					Nom : <input id="TB_assets_name" class="TB_editProperty needSelectedAssets" type="text" />
					<form id="F_changeAssetImage" method="post" action="./webservice/uploadImages.php<?php echo (isset($_GET['dev'])) ? '?dev' : ''; ?>">
						<input id="B_changeAssetImage" class="needSelectedAssets" name="imageUpload" type="file" disabled="true" />
						<br/>
						<input id="H_selectedAssetsId" name="selectedAssetsId" type="hidden" value="0" />
						<input id="S_changeAssetImage" class="needSelectedAssets" type="submit" value="Envoyer l'image" />
					</form>
				</div>	
				<div id="D_assetsList" class="list" >
					Chargement en cours...
				</div>
			</div>
			<!--<canvas class="container" id="C_assetImage" height="200" width="200" ></canvas>-->
			<div class="container" id="C_assetImage" ></div>
		</div>
		
		<!--OPTION MENU-->
		<div id="listOptionMenu" class="optionMenu">
			<div id="addListItemOptionMenu" >Ajouter</div>
			<div id="removeListItemOptionMenu" >Supprimer</div>
		</div>
		
		<div id="hitboxPointsOptionMenu" class="optionMenu">
			<div id="addPointOptionMenu" >Ajouter</div>
			<div id="removePointOptionMenu" >Supprimer</div>
		</div>
		
		<div id="layoutElementsOptionMenu" class="optionMenu">
			<div id="addLayoutElementOptionMenu" >Ajouter</div>
			<div id="removeLayoutElementOptionMenu" >Supprimer</div>
		</div>		
		<div id="layoutGroupsOptionMenu" class="optionMenu">
			<div id="addLayoutGroupOptionMenu" >Ajouter</div>
			<div id="removeLayoutGroupOptionMenu" >Supprimer</div>
		</div>
		
		<div id="shadow"></div>
		<img id="loadingImage" src="images/loading.gif" alt="loading"/>
		
		<script src="./includes/jquery-2.0.3.min.js"></script>
		<script src="./includes/jquery-ui.min.js"></script>
		<script src="./includes/kinetic-v5.0.1.min.js"></script>
		
		<script src="./includes/main.js"></script>
		<script src="./includes/general-functions.js"></script>
		<script src="./includes/lists-functions.js"></script>
		<script src="./includes/draw.js"></script>
		<script src="./includes/events.js"></script>
		
	</body>
</html>